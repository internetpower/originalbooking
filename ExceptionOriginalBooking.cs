﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.ExceptionOriginalBooking
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using System;

namespace OriginalBooking
{
  public class ExceptionOriginalBooking : Exception
  {
    public ExceptionOriginalBooking(string message)
      : base(message)
    {
    }
  }
}
