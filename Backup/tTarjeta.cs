﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.tTarjeta
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

namespace OriginalBooking
{
  public enum tTarjeta
  {
    ccDinerClub,
    ccAmericanExpress,
    ccJCB,
    ccCarteBlanche,
    ccVisa,
    ccMasterCard,
    ccAustralianBankCard,
    ccDiscover,
  }
}
