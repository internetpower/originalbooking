﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.ProjectInstaller
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace OriginalBooking
{
  [RunInstaller(true)]
  public class ProjectInstaller : Installer
  {
    private IContainer components = (IContainer) null;
    private ServiceProcessInstaller serviceProcessInstaller1;
    private ServiceInstaller serviceInstaller1;

    public ProjectInstaller()
    {
      this.InitializeComponent();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.serviceProcessInstaller1 = new ServiceProcessInstaller();
      this.serviceInstaller1 = new ServiceInstaller();
      this.serviceProcessInstaller1.Account = ServiceAccount.LocalService;
      this.serviceProcessInstaller1.Password = (string) null;
      this.serviceProcessInstaller1.Username = (string) null;
      this.serviceInstaller1.ServiceName = "UniVisit_OriginalBooking";
      this.serviceInstaller1.StartType = ServiceStartMode.Automatic;
      this.Installers.AddRange(new Installer[2]
      {
        (Installer) this.serviceProcessInstaller1,
        (Installer) this.serviceInstaller1
      });
    }
  }
}
