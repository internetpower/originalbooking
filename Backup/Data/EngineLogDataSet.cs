﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.Data.EngineLogDataSet
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace OriginalBooking.Data
{
  [XmlRoot("EngineLogDataSet")]
  [HelpKeyword("vs.data.DataSet")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  [ToolboxItem(true)]
  [XmlSchemaProvider("GetTypedDataSetSchema")]
  [Serializable]
  public class EngineLogDataSet : DataSet
  {
    private SchemaSerializationMode _schemaSerializationMode = SchemaSerializationMode.IncludeSchema;
    private EngineLogDataSet.OriginalBookingDataTable tableOriginalBooking;
    private EngineLogDataSet.OriginalBookingExistsForFileNameDataTable tableOriginalBookingExistsForFileName;
    private EngineLogDataSet.EngineLogDataTable tableEngineLog;
    private EngineLogDataSet.RequestWebSiteDataTable tableRequestWebSite;
    private EngineLogDataSet.RequestAgencyDataTable tableRequestAgency;

    [DebuggerNonUserCode]
    public EngineLogDataSet()
    {
      this.BeginInit();
      this.InitClass();
      CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
      base.Tables.CollectionChanged += changeEventHandler;
      base.Relations.CollectionChanged += changeEventHandler;
      this.EndInit();
    }

    [DebuggerNonUserCode]
    protected EngineLogDataSet(SerializationInfo info, StreamingContext context)
      : base(info, context, false)
    {
      if (this.IsBinarySerialized(info, context))
      {
        this.InitVars(false);
        CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
        this.Tables.CollectionChanged += changeEventHandler;
        this.Relations.CollectionChanged += changeEventHandler;
      }
      else
      {
        string s = (string) info.GetValue("XmlSchema", typeof (string));
        if (this.DetermineSchemaSerializationMode(info, context) == SchemaSerializationMode.IncludeSchema)
        {
          DataSet dataSet = new DataSet();
          dataSet.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
          if (dataSet.Tables[nameof (OriginalBooking)] != null)
            base.Tables.Add((DataTable) new EngineLogDataSet.OriginalBookingDataTable(dataSet.Tables[nameof (OriginalBooking)]));
          if (dataSet.Tables[nameof (OriginalBookingExistsForFileName)] != null)
            base.Tables.Add((DataTable) new EngineLogDataSet.OriginalBookingExistsForFileNameDataTable(dataSet.Tables[nameof (OriginalBookingExistsForFileName)]));
          if (dataSet.Tables[nameof (EngineLog)] != null)
            base.Tables.Add((DataTable) new EngineLogDataSet.EngineLogDataTable(dataSet.Tables[nameof (EngineLog)]));
          if (dataSet.Tables[nameof (RequestWebSite)] != null)
            base.Tables.Add((DataTable) new EngineLogDataSet.RequestWebSiteDataTable(dataSet.Tables[nameof (RequestWebSite)]));
          if (dataSet.Tables[nameof (RequestAgency)] != null)
            base.Tables.Add((DataTable) new EngineLogDataSet.RequestAgencyDataTable(dataSet.Tables[nameof (RequestAgency)]));
          this.DataSetName = dataSet.DataSetName;
          this.Prefix = dataSet.Prefix;
          this.Namespace = dataSet.Namespace;
          this.Locale = dataSet.Locale;
          this.CaseSensitive = dataSet.CaseSensitive;
          this.EnforceConstraints = dataSet.EnforceConstraints;
          this.Merge(dataSet, false, MissingSchemaAction.Add);
          this.InitVars();
        }
        else
          this.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
        this.GetSerializationData(info, context);
        CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
        base.Tables.CollectionChanged += changeEventHandler;
        this.Relations.CollectionChanged += changeEventHandler;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [Browsable(false)]
    public EngineLogDataSet.OriginalBookingDataTable OriginalBooking
    {
      get
      {
        return this.tableOriginalBooking;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [DebuggerNonUserCode]
    [Browsable(false)]
    public EngineLogDataSet.OriginalBookingExistsForFileNameDataTable OriginalBookingExistsForFileName
    {
      get
      {
        return this.tableOriginalBookingExistsForFileName;
      }
    }

    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [Browsable(false)]
    public EngineLogDataSet.EngineLogDataTable EngineLog
    {
      get
      {
        return this.tableEngineLog;
      }
    }

    [DebuggerNonUserCode]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public EngineLogDataSet.RequestWebSiteDataTable RequestWebSite
    {
      get
      {
        return this.tableRequestWebSite;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [Browsable(false)]
    [DebuggerNonUserCode]
    public EngineLogDataSet.RequestAgencyDataTable RequestAgency
    {
      get
      {
        return this.tableRequestAgency;
      }
    }

    [Browsable(true)]
    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    public override SchemaSerializationMode SchemaSerializationMode
    {
      get
      {
        return this._schemaSerializationMode;
      }
      set
      {
        this._schemaSerializationMode = value;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [DebuggerNonUserCode]
    public new DataTableCollection Tables
    {
      get
      {
        return base.Tables;
      }
    }

    [DebuggerNonUserCode]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public new DataRelationCollection Relations
    {
      get
      {
        return base.Relations;
      }
    }

    [DebuggerNonUserCode]
    protected override void InitializeDerivedDataSet()
    {
      this.BeginInit();
      this.InitClass();
      this.EndInit();
    }

    [DebuggerNonUserCode]
    public override DataSet Clone()
    {
      EngineLogDataSet engineLogDataSet = (EngineLogDataSet) base.Clone();
      engineLogDataSet.InitVars();
      engineLogDataSet.SchemaSerializationMode = this.SchemaSerializationMode;
      return (DataSet) engineLogDataSet;
    }

    [DebuggerNonUserCode]
    protected override bool ShouldSerializeTables()
    {
      return false;
    }

    [DebuggerNonUserCode]
    protected override bool ShouldSerializeRelations()
    {
      return false;
    }

    [DebuggerNonUserCode]
    protected override void ReadXmlSerializable(XmlReader reader)
    {
      if (this.DetermineSchemaSerializationMode(reader) == SchemaSerializationMode.IncludeSchema)
      {
        this.Reset();
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(reader);
        if (dataSet.Tables["OriginalBooking"] != null)
          base.Tables.Add((DataTable) new EngineLogDataSet.OriginalBookingDataTable(dataSet.Tables["OriginalBooking"]));
        if (dataSet.Tables["OriginalBookingExistsForFileName"] != null)
          base.Tables.Add((DataTable) new EngineLogDataSet.OriginalBookingExistsForFileNameDataTable(dataSet.Tables["OriginalBookingExistsForFileName"]));
        if (dataSet.Tables["EngineLog"] != null)
          base.Tables.Add((DataTable) new EngineLogDataSet.EngineLogDataTable(dataSet.Tables["EngineLog"]));
        if (dataSet.Tables["RequestWebSite"] != null)
          base.Tables.Add((DataTable) new EngineLogDataSet.RequestWebSiteDataTable(dataSet.Tables["RequestWebSite"]));
        if (dataSet.Tables["RequestAgency"] != null)
          base.Tables.Add((DataTable) new EngineLogDataSet.RequestAgencyDataTable(dataSet.Tables["RequestAgency"]));
        this.DataSetName = dataSet.DataSetName;
        this.Prefix = dataSet.Prefix;
        this.Namespace = dataSet.Namespace;
        this.Locale = dataSet.Locale;
        this.CaseSensitive = dataSet.CaseSensitive;
        this.EnforceConstraints = dataSet.EnforceConstraints;
        this.Merge(dataSet, false, MissingSchemaAction.Add);
        this.InitVars();
      }
      else
      {
        int num = (int) this.ReadXml(reader);
        this.InitVars();
      }
    }

    [DebuggerNonUserCode]
    protected override XmlSchema GetSchemaSerializable()
    {
      MemoryStream memoryStream = new MemoryStream();
      this.WriteXmlSchema((XmlWriter) new XmlTextWriter((Stream) memoryStream, (Encoding) null));
      memoryStream.Position = 0L;
      return XmlSchema.Read((XmlReader) new XmlTextReader((Stream) memoryStream), (ValidationEventHandler) null);
    }

    [DebuggerNonUserCode]
    internal void InitVars()
    {
      this.InitVars(true);
    }

    [DebuggerNonUserCode]
    internal void InitVars(bool initTable)
    {
      this.tableOriginalBooking = (EngineLogDataSet.OriginalBookingDataTable) base.Tables["OriginalBooking"];
      if (initTable && this.tableOriginalBooking != null)
        this.tableOriginalBooking.InitVars();
      this.tableOriginalBookingExistsForFileName = (EngineLogDataSet.OriginalBookingExistsForFileNameDataTable) base.Tables["OriginalBookingExistsForFileName"];
      if (initTable && this.tableOriginalBookingExistsForFileName != null)
        this.tableOriginalBookingExistsForFileName.InitVars();
      this.tableEngineLog = (EngineLogDataSet.EngineLogDataTable) base.Tables["EngineLog"];
      if (initTable && this.tableEngineLog != null)
        this.tableEngineLog.InitVars();
      this.tableRequestWebSite = (EngineLogDataSet.RequestWebSiteDataTable) base.Tables["RequestWebSite"];
      if (initTable && this.tableRequestWebSite != null)
        this.tableRequestWebSite.InitVars();
      this.tableRequestAgency = (EngineLogDataSet.RequestAgencyDataTable) base.Tables["RequestAgency"];
      if (!initTable || this.tableRequestAgency == null)
        return;
      this.tableRequestAgency.InitVars();
    }

    [DebuggerNonUserCode]
    private void InitClass()
    {
      this.DataSetName = nameof (EngineLogDataSet);
      this.Prefix = "";
      this.Namespace = "http://tempuri.org/EngineLogDataSet.xsd";
      this.EnforceConstraints = true;
      this.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
      this.tableOriginalBooking = new EngineLogDataSet.OriginalBookingDataTable();
      base.Tables.Add((DataTable) this.tableOriginalBooking);
      this.tableOriginalBookingExistsForFileName = new EngineLogDataSet.OriginalBookingExistsForFileNameDataTable();
      base.Tables.Add((DataTable) this.tableOriginalBookingExistsForFileName);
      this.tableEngineLog = new EngineLogDataSet.EngineLogDataTable();
      base.Tables.Add((DataTable) this.tableEngineLog);
      this.tableRequestWebSite = new EngineLogDataSet.RequestWebSiteDataTable();
      base.Tables.Add((DataTable) this.tableRequestWebSite);
      this.tableRequestAgency = new EngineLogDataSet.RequestAgencyDataTable();
      base.Tables.Add((DataTable) this.tableRequestAgency);
    }

    [DebuggerNonUserCode]
    private bool ShouldSerializeOriginalBooking()
    {
      return false;
    }

    [DebuggerNonUserCode]
    private bool ShouldSerializeOriginalBookingExistsForFileName()
    {
      return false;
    }

    [DebuggerNonUserCode]
    private bool ShouldSerializeEngineLog()
    {
      return false;
    }

    [DebuggerNonUserCode]
    private bool ShouldSerializeRequestWebSite()
    {
      return false;
    }

    [DebuggerNonUserCode]
    private bool ShouldSerializeRequestAgency()
    {
      return false;
    }

    [DebuggerNonUserCode]
    private void SchemaChanged(object sender, CollectionChangeEventArgs e)
    {
      if (e.Action != CollectionChangeAction.Remove)
        return;
      this.InitVars();
    }

    [DebuggerNonUserCode]
    public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
    {
      EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
      XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
      XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
      xmlSchemaSequence.Items.Add((XmlSchemaObject) new XmlSchemaAny()
      {
        Namespace = engineLogDataSet.Namespace
      });
      schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
      XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
      if (xs.Contains(schemaSerializable.TargetNamespace))
      {
        MemoryStream memoryStream1 = new MemoryStream();
        MemoryStream memoryStream2 = new MemoryStream();
        try
        {
          schemaSerializable.Write((Stream) memoryStream1);
          foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
          {
            memoryStream2.SetLength(0L);
            schema.Write((Stream) memoryStream2);
            if (memoryStream1.Length == memoryStream2.Length)
            {
              memoryStream1.Position = 0L;
              memoryStream2.Position = 0L;
              do
                ;
              while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
              if (memoryStream1.Position == memoryStream1.Length)
                return schemaComplexType;
            }
          }
        }
        finally
        {
          if (memoryStream1 != null)
            memoryStream1.Close();
          if (memoryStream2 != null)
            memoryStream2.Close();
        }
      }
      xs.Add(schemaSerializable);
      return schemaComplexType;
    }

    public delegate void OriginalBookingRowChangeEventHandler(object sender, EngineLogDataSet.OriginalBookingRowChangeEvent e);

    public delegate void OriginalBookingExistsForFileNameRowChangeEventHandler(object sender, EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEvent e);

    public delegate void EngineLogRowChangeEventHandler(object sender, EngineLogDataSet.EngineLogRowChangeEvent e);

    public delegate void RequestWebSiteRowChangeEventHandler(object sender, EngineLogDataSet.RequestWebSiteRowChangeEvent e);

    public delegate void RequestAgencyRowChangeEventHandler(object sender, EngineLogDataSet.RequestAgencyRowChangeEvent e);

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class OriginalBookingDataTable : TypedTableBase<EngineLogDataSet.OriginalBookingRow>
    {
      private DataColumn columnOrginalBookingID;
      private DataColumn columnFileName;
      private DataColumn columnFileContent;
      private DataColumn columnUrl;
      private DataColumn columnStatus;
      private DataColumn columnID;
      private DataColumn columnDate;
      private DataColumn columnType;
      private DataColumn columnReservationNumberOrginal;
      private DataColumn columnReservationNumberUv;
      private DataColumn columnStatusText;
      private DataColumn columnHotelIdUv;
      private DataColumn columnHotelName;
      private DataColumn columnReservationType;
      private DataColumn columnAgencyCode;
      private DataColumn columnAgencyName;
      private DataColumn columnAgentName;
      private DataColumn columnSetupAgent;
      private DataColumn columnReservationTypeCode;

      [DebuggerNonUserCode]
      public OriginalBookingDataTable()
      {
        this.TableName = "OriginalBooking";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      internal OriginalBookingDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      protected OriginalBookingDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      public DataColumn OrginalBookingIDColumn
      {
        get
        {
          return this.columnOrginalBookingID;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn FileNameColumn
      {
        get
        {
          return this.columnFileName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn FileContentColumn
      {
        get
        {
          return this.columnFileContent;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn UrlColumn
      {
        get
        {
          return this.columnUrl;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn StatusColumn
      {
        get
        {
          return this.columnStatus;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn IDColumn
      {
        get
        {
          return this.columnID;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn DateColumn
      {
        get
        {
          return this.columnDate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn TypeColumn
      {
        get
        {
          return this.columnType;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationNumberOrginalColumn
      {
        get
        {
          return this.columnReservationNumberOrginal;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationNumberUvColumn
      {
        get
        {
          return this.columnReservationNumberUv;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn StatusTextColumn
      {
        get
        {
          return this.columnStatusText;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn HotelIdUvColumn
      {
        get
        {
          return this.columnHotelIdUv;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn HotelNameColumn
      {
        get
        {
          return this.columnHotelName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationTypeColumn
      {
        get
        {
          return this.columnReservationType;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AgencyCodeColumn
      {
        get
        {
          return this.columnAgencyCode;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AgencyNameColumn
      {
        get
        {
          return this.columnAgencyName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AgentNameColumn
      {
        get
        {
          return this.columnAgentName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn SetupAgentColumn
      {
        get
        {
          return this.columnSetupAgent;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationTypeCodeColumn
      {
        get
        {
          return this.columnReservationTypeCode;
        }
      }

      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingRow this[int index]
      {
        get
        {
          return (EngineLogDataSet.OriginalBookingRow) this.Rows[index];
        }
      }

      public event EngineLogDataSet.OriginalBookingRowChangeEventHandler OriginalBookingRowChanging;

      public event EngineLogDataSet.OriginalBookingRowChangeEventHandler OriginalBookingRowChanged;

      public event EngineLogDataSet.OriginalBookingRowChangeEventHandler OriginalBookingRowDeleting;

      public event EngineLogDataSet.OriginalBookingRowChangeEventHandler OriginalBookingRowDeleted;

      [DebuggerNonUserCode]
      public void AddOriginalBookingRow(EngineLogDataSet.OriginalBookingRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingRow AddOriginalBookingRow(string FileName, string FileContent, string Url, int Status, string ID, DateTime Date, string Type, string ReservationNumberOrginal, string ReservationNumberUv, string StatusText, int HotelIdUv, string HotelName, int ReservationType, string AgencyCode, string AgencyName, string AgentName, string SetupAgent, string ReservationTypeCode)
      {
        EngineLogDataSet.OriginalBookingRow originalBookingRow = (EngineLogDataSet.OriginalBookingRow) this.NewRow();
        object[] objArray = new object[19]
        {
          null,
          (object) FileName,
          (object) FileContent,
          (object) Url,
          (object) Status,
          (object) ID,
          (object) Date,
          (object) Type,
          (object) ReservationNumberOrginal,
          (object) ReservationNumberUv,
          (object) StatusText,
          (object) HotelIdUv,
          (object) HotelName,
          (object) ReservationType,
          (object) AgencyCode,
          (object) AgencyName,
          (object) AgentName,
          (object) SetupAgent,
          (object) ReservationTypeCode
        };
        originalBookingRow.ItemArray = objArray;
        this.Rows.Add((DataRow) originalBookingRow);
        return originalBookingRow;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingRow FindByOrginalBookingID(int OrginalBookingID)
      {
        return (EngineLogDataSet.OriginalBookingRow) this.Rows.Find(new object[1]
        {
          (object) OrginalBookingID
        });
      }

      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        EngineLogDataSet.OriginalBookingDataTable bookingDataTable = (EngineLogDataSet.OriginalBookingDataTable) base.Clone();
        bookingDataTable.InitVars();
        return (DataTable) bookingDataTable;
      }

      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new EngineLogDataSet.OriginalBookingDataTable();
      }

      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnOrginalBookingID = this.Columns["OrginalBookingID"];
        this.columnFileName = this.Columns["FileName"];
        this.columnFileContent = this.Columns["FileContent"];
        this.columnUrl = this.Columns["Url"];
        this.columnStatus = this.Columns["Status"];
        this.columnID = this.Columns["ID"];
        this.columnDate = this.Columns["Date"];
        this.columnType = this.Columns["Type"];
        this.columnReservationNumberOrginal = this.Columns["ReservationNumberOrginal"];
        this.columnReservationNumberUv = this.Columns["ReservationNumberUv"];
        this.columnStatusText = this.Columns["StatusText"];
        this.columnHotelIdUv = this.Columns["HotelIdUv"];
        this.columnHotelName = this.Columns["HotelName"];
        this.columnReservationType = this.Columns["ReservationType"];
        this.columnAgencyCode = this.Columns["AgencyCode"];
        this.columnAgencyName = this.Columns["AgencyName"];
        this.columnAgentName = this.Columns["AgentName"];
        this.columnSetupAgent = this.Columns["SetupAgent"];
        this.columnReservationTypeCode = this.Columns["ReservationTypeCode"];
      }

      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnOrginalBookingID = new DataColumn("OrginalBookingID", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnOrginalBookingID);
        this.columnFileName = new DataColumn("FileName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFileName);
        this.columnFileContent = new DataColumn("FileContent", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFileContent);
        this.columnUrl = new DataColumn("Url", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnUrl);
        this.columnStatus = new DataColumn("Status", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnStatus);
        this.columnID = new DataColumn("ID", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnID);
        this.columnDate = new DataColumn("Date", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDate);
        this.columnType = new DataColumn("Type", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnType);
        this.columnReservationNumberOrginal = new DataColumn("ReservationNumberOrginal", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationNumberOrginal);
        this.columnReservationNumberUv = new DataColumn("ReservationNumberUv", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationNumberUv);
        this.columnStatusText = new DataColumn("StatusText", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnStatusText);
        this.columnHotelIdUv = new DataColumn("HotelIdUv", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnHotelIdUv);
        this.columnHotelName = new DataColumn("HotelName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnHotelName);
        this.columnReservationType = new DataColumn("ReservationType", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationType);
        this.columnAgencyCode = new DataColumn("AgencyCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAgencyCode);
        this.columnAgencyName = new DataColumn("AgencyName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAgencyName);
        this.columnAgentName = new DataColumn("AgentName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAgentName);
        this.columnSetupAgent = new DataColumn("SetupAgent", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSetupAgent);
        this.columnReservationTypeCode = new DataColumn("ReservationTypeCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationTypeCode);
        this.Constraints.Add((Constraint) new UniqueConstraint("Constraint1", new DataColumn[1]
        {
          this.columnOrginalBookingID
        }, true));
        this.columnOrginalBookingID.AutoIncrement = true;
        this.columnOrginalBookingID.AutoIncrementSeed = -1L;
        this.columnOrginalBookingID.AutoIncrementStep = -1L;
        this.columnOrginalBookingID.AllowDBNull = false;
        this.columnOrginalBookingID.ReadOnly = true;
        this.columnOrginalBookingID.Unique = true;
        this.columnFileName.MaxLength = 200;
        this.columnFileContent.MaxLength = 2000;
        this.columnUrl.MaxLength = 1000;
        this.columnID.MaxLength = 20;
        this.columnType.MaxLength = 20;
        this.columnReservationNumberOrginal.MaxLength = 20;
        this.columnReservationNumberUv.MaxLength = 20;
        this.columnStatusText.MaxLength = 500;
        this.columnHotelName.MaxLength = 100;
        this.columnAgencyCode.MaxLength = 20;
        this.columnAgencyName.MaxLength = 100;
        this.columnAgentName.MaxLength = 100;
        this.columnSetupAgent.MaxLength = 50;
        this.columnReservationTypeCode.MaxLength = 50;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingRow NewOriginalBookingRow()
      {
        return (EngineLogDataSet.OriginalBookingRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new EngineLogDataSet.OriginalBookingRow(builder);
      }

      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (EngineLogDataSet.OriginalBookingRow);
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.OriginalBookingRowChanged == null)
          return;
        this.OriginalBookingRowChanged((object) this, new EngineLogDataSet.OriginalBookingRowChangeEvent((EngineLogDataSet.OriginalBookingRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.OriginalBookingRowChanging == null)
          return;
        this.OriginalBookingRowChanging((object) this, new EngineLogDataSet.OriginalBookingRowChangeEvent((EngineLogDataSet.OriginalBookingRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.OriginalBookingRowDeleted == null)
          return;
        this.OriginalBookingRowDeleted((object) this, new EngineLogDataSet.OriginalBookingRowChangeEvent((EngineLogDataSet.OriginalBookingRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.OriginalBookingRowDeleting == null)
          return;
        this.OriginalBookingRowDeleting((object) this, new EngineLogDataSet.OriginalBookingRowChangeEvent((EngineLogDataSet.OriginalBookingRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      public void RemoveOriginalBookingRow(EngineLogDataSet.OriginalBookingRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = engineLogDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (OriginalBookingDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [Serializable]
    public class OriginalBookingExistsForFileNameDataTable : TypedTableBase<EngineLogDataSet.OriginalBookingExistsForFileNameRow>
    {
      private DataColumn columnCount;

      [DebuggerNonUserCode]
      public OriginalBookingExistsForFileNameDataTable()
      {
        this.TableName = "OriginalBookingExistsForFileName";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      internal OriginalBookingExistsForFileNameDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      protected OriginalBookingExistsForFileNameDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      public DataColumn CountColumn
      {
        get
        {
          return this.columnCount;
        }
      }

      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingExistsForFileNameRow this[int index]
      {
        get
        {
          return (EngineLogDataSet.OriginalBookingExistsForFileNameRow) this.Rows[index];
        }
      }

      public event EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEventHandler OriginalBookingExistsForFileNameRowChanging;

      public event EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEventHandler OriginalBookingExistsForFileNameRowChanged;

      public event EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEventHandler OriginalBookingExistsForFileNameRowDeleting;

      public event EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEventHandler OriginalBookingExistsForFileNameRowDeleted;

      [DebuggerNonUserCode]
      public void AddOriginalBookingExistsForFileNameRow(EngineLogDataSet.OriginalBookingExistsForFileNameRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingExistsForFileNameRow AddOriginalBookingExistsForFileNameRow(int Count)
      {
        EngineLogDataSet.OriginalBookingExistsForFileNameRow existsForFileNameRow = (EngineLogDataSet.OriginalBookingExistsForFileNameRow) this.NewRow();
        object[] objArray = new object[1]{ (object) Count };
        existsForFileNameRow.ItemArray = objArray;
        this.Rows.Add((DataRow) existsForFileNameRow);
        return existsForFileNameRow;
      }

      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        EngineLogDataSet.OriginalBookingExistsForFileNameDataTable fileNameDataTable = (EngineLogDataSet.OriginalBookingExistsForFileNameDataTable) base.Clone();
        fileNameDataTable.InitVars();
        return (DataTable) fileNameDataTable;
      }

      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new EngineLogDataSet.OriginalBookingExistsForFileNameDataTable();
      }

      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnCount = this.Columns["Count"];
      }

      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnCount = new DataColumn("Count", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCount);
        this.columnCount.ReadOnly = true;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingExistsForFileNameRow NewOriginalBookingExistsForFileNameRow()
      {
        return (EngineLogDataSet.OriginalBookingExistsForFileNameRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new EngineLogDataSet.OriginalBookingExistsForFileNameRow(builder);
      }

      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (EngineLogDataSet.OriginalBookingExistsForFileNameRow);
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.OriginalBookingExistsForFileNameRowChanged == null)
          return;
        this.OriginalBookingExistsForFileNameRowChanged((object) this, new EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEvent((EngineLogDataSet.OriginalBookingExistsForFileNameRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.OriginalBookingExistsForFileNameRowChanging == null)
          return;
        this.OriginalBookingExistsForFileNameRowChanging((object) this, new EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEvent((EngineLogDataSet.OriginalBookingExistsForFileNameRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.OriginalBookingExistsForFileNameRowDeleted == null)
          return;
        this.OriginalBookingExistsForFileNameRowDeleted((object) this, new EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEvent((EngineLogDataSet.OriginalBookingExistsForFileNameRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.OriginalBookingExistsForFileNameRowDeleting == null)
          return;
        this.OriginalBookingExistsForFileNameRowDeleting((object) this, new EngineLogDataSet.OriginalBookingExistsForFileNameRowChangeEvent((EngineLogDataSet.OriginalBookingExistsForFileNameRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      public void RemoveOriginalBookingExistsForFileNameRow(EngineLogDataSet.OriginalBookingExistsForFileNameRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = engineLogDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (OriginalBookingExistsForFileNameDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [XmlSchemaProvider("GetTypedTableSchema")]
    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [Serializable]
    public class EngineLogDataTable : TypedTableBase<EngineLogDataSet.EngineLogRow>
    {
      private DataColumn columnFileName;
      private DataColumn columnContent;
      private DataColumn columnUrl;
      private DataColumn columnStatus;
      private DataColumn columnID;
      private DataColumn columnDate;
      private DataColumn columnReservationType;

      [DebuggerNonUserCode]
      public EngineLogDataTable()
      {
        this.TableName = "EngineLog";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      internal EngineLogDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      protected EngineLogDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      public DataColumn FileNameColumn
      {
        get
        {
          return this.columnFileName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ContentColumn
      {
        get
        {
          return this.columnContent;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn UrlColumn
      {
        get
        {
          return this.columnUrl;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn StatusColumn
      {
        get
        {
          return this.columnStatus;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn IDColumn
      {
        get
        {
          return this.columnID;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn DateColumn
      {
        get
        {
          return this.columnDate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationTypeColumn
      {
        get
        {
          return this.columnReservationType;
        }
      }

      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.EngineLogRow this[int index]
      {
        get
        {
          return (EngineLogDataSet.EngineLogRow) this.Rows[index];
        }
      }

      public event EngineLogDataSet.EngineLogRowChangeEventHandler EngineLogRowChanging;

      public event EngineLogDataSet.EngineLogRowChangeEventHandler EngineLogRowChanged;

      public event EngineLogDataSet.EngineLogRowChangeEventHandler EngineLogRowDeleting;

      public event EngineLogDataSet.EngineLogRowChangeEventHandler EngineLogRowDeleted;

      [DebuggerNonUserCode]
      public void AddEngineLogRow(EngineLogDataSet.EngineLogRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.EngineLogRow AddEngineLogRow(string FileName, string Content, string Url, int Status, string ID, DateTime Date, int ReservationType)
      {
        EngineLogDataSet.EngineLogRow engineLogRow = (EngineLogDataSet.EngineLogRow) this.NewRow();
        object[] objArray = new object[7]
        {
          (object) FileName,
          (object) Content,
          (object) Url,
          (object) Status,
          (object) ID,
          (object) Date,
          (object) ReservationType
        };
        engineLogRow.ItemArray = objArray;
        this.Rows.Add((DataRow) engineLogRow);
        return engineLogRow;
      }

      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        EngineLogDataSet.EngineLogDataTable engineLogDataTable = (EngineLogDataSet.EngineLogDataTable) base.Clone();
        engineLogDataTable.InitVars();
        return (DataTable) engineLogDataTable;
      }

      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new EngineLogDataSet.EngineLogDataTable();
      }

      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnFileName = this.Columns["FileName"];
        this.columnContent = this.Columns["Content"];
        this.columnUrl = this.Columns["Url"];
        this.columnStatus = this.Columns["Status"];
        this.columnID = this.Columns["ID"];
        this.columnDate = this.Columns["Date"];
        this.columnReservationType = this.Columns["ReservationType"];
      }

      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnFileName = new DataColumn("FileName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFileName);
        this.columnContent = new DataColumn("Content", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnContent);
        this.columnUrl = new DataColumn("Url", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnUrl);
        this.columnStatus = new DataColumn("Status", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnStatus);
        this.columnID = new DataColumn("ID", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnID);
        this.columnDate = new DataColumn("Date", typeof (DateTime), (string) null, MappingType.Element);
        this.Columns.Add(this.columnDate);
        this.columnReservationType = new DataColumn("ReservationType", typeof (int), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationType);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.EngineLogRow NewEngineLogRow()
      {
        return (EngineLogDataSet.EngineLogRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new EngineLogDataSet.EngineLogRow(builder);
      }

      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (EngineLogDataSet.EngineLogRow);
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.EngineLogRowChanged == null)
          return;
        this.EngineLogRowChanged((object) this, new EngineLogDataSet.EngineLogRowChangeEvent((EngineLogDataSet.EngineLogRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.EngineLogRowChanging == null)
          return;
        this.EngineLogRowChanging((object) this, new EngineLogDataSet.EngineLogRowChangeEvent((EngineLogDataSet.EngineLogRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.EngineLogRowDeleted == null)
          return;
        this.EngineLogRowDeleted((object) this, new EngineLogDataSet.EngineLogRowChangeEvent((EngineLogDataSet.EngineLogRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.EngineLogRowDeleting == null)
          return;
        this.EngineLogRowDeleting((object) this, new EngineLogDataSet.EngineLogRowChangeEvent((EngineLogDataSet.EngineLogRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      public void RemoveEngineLogRow(EngineLogDataSet.EngineLogRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = engineLogDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (EngineLogDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class RequestWebSiteDataTable : TypedTableBase<EngineLogDataSet.RequestWebSiteRow>
    {
      private DataColumn columnLastName;
      private DataColumn columnFirstName;
      private DataColumn columnTelephone;
      private DataColumn columnAdress;
      private DataColumn columnCity;
      private DataColumn columnEstate;
      private DataColumn columnCountry;
      private DataColumn columnPostalCode;
      private DataColumn columnEmail;
      private DataColumn columnConfirmationNumber;
      private DataColumn columnCheckIn;
      private DataColumn columnCheckOut;
      private DataColumn columnSetupAgent;
      private DataColumn columnReservationStatus;
      private DataColumn columnGroup;
      private DataColumn columnHotel;
      private DataColumn columnNights;
      private DataColumn columnTotal;
      private DataColumn columnRate;
      private DataColumn columnBalance;
      private DataColumn columnCurrencyCode;
      private DataColumn columnLastNight;
      private DataColumn columnFreeNight;
      private DataColumn columnAirportTranfer;
      private DataColumn columnAdults;
      private DataColumn columnChildren;
      private DataColumn columnSpecialRequest;
      private DataColumn columnRoomName;
      private DataColumn columnRooms;
      private DataColumn columnLastCurrencyCode;
      private DataColumn columnSource;

      [DebuggerNonUserCode]
      public RequestWebSiteDataTable()
      {
        this.TableName = "RequestWebSite";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      internal RequestWebSiteDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      protected RequestWebSiteDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      public DataColumn LastNameColumn
      {
        get
        {
          return this.columnLastName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn FirstNameColumn
      {
        get
        {
          return this.columnFirstName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn TelephoneColumn
      {
        get
        {
          return this.columnTelephone;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AdressColumn
      {
        get
        {
          return this.columnAdress;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CityColumn
      {
        get
        {
          return this.columnCity;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn EstateColumn
      {
        get
        {
          return this.columnEstate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CountryColumn
      {
        get
        {
          return this.columnCountry;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn PostalCodeColumn
      {
        get
        {
          return this.columnPostalCode;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn EmailColumn
      {
        get
        {
          return this.columnEmail;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ConfirmationNumberColumn
      {
        get
        {
          return this.columnConfirmationNumber;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CheckInColumn
      {
        get
        {
          return this.columnCheckIn;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CheckOutColumn
      {
        get
        {
          return this.columnCheckOut;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn SetupAgentColumn
      {
        get
        {
          return this.columnSetupAgent;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationStatusColumn
      {
        get
        {
          return this.columnReservationStatus;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn GroupColumn
      {
        get
        {
          return this.columnGroup;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn HotelColumn
      {
        get
        {
          return this.columnHotel;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn NightsColumn
      {
        get
        {
          return this.columnNights;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn TotalColumn
      {
        get
        {
          return this.columnTotal;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RateColumn
      {
        get
        {
          return this.columnRate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn BalanceColumn
      {
        get
        {
          return this.columnBalance;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CurrencyCodeColumn
      {
        get
        {
          return this.columnCurrencyCode;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn LastNightColumn
      {
        get
        {
          return this.columnLastNight;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn FreeNightColumn
      {
        get
        {
          return this.columnFreeNight;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AirportTranferColumn
      {
        get
        {
          return this.columnAirportTranfer;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AdultsColumn
      {
        get
        {
          return this.columnAdults;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ChildrenColumn
      {
        get
        {
          return this.columnChildren;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn SpecialRequestColumn
      {
        get
        {
          return this.columnSpecialRequest;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RoomNameColumn
      {
        get
        {
          return this.columnRoomName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RoomsColumn
      {
        get
        {
          return this.columnRooms;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn LastCurrencyCodeColumn
      {
        get
        {
          return this.columnLastCurrencyCode;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn SourceColumn
      {
        get
        {
          return this.columnSource;
        }
      }

      [Browsable(false)]
      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestWebSiteRow this[int index]
      {
        get
        {
          return (EngineLogDataSet.RequestWebSiteRow) this.Rows[index];
        }
      }

      public event EngineLogDataSet.RequestWebSiteRowChangeEventHandler RequestWebSiteRowChanging;

      public event EngineLogDataSet.RequestWebSiteRowChangeEventHandler RequestWebSiteRowChanged;

      public event EngineLogDataSet.RequestWebSiteRowChangeEventHandler RequestWebSiteRowDeleting;

      public event EngineLogDataSet.RequestWebSiteRowChangeEventHandler RequestWebSiteRowDeleted;

      [DebuggerNonUserCode]
      public void AddRequestWebSiteRow(EngineLogDataSet.RequestWebSiteRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestWebSiteRow AddRequestWebSiteRow(string LastName, string FirstName, string Telephone, string Adress, string City, string Estate, string Country, string PostalCode, string Email, string ConfirmationNumber, string CheckIn, string CheckOut, string SetupAgent, string ReservationStatus, string Group, string Hotel, string Nights, string Total, string Rate, string Balance, string CurrencyCode, string LastNight, string FreeNight, string AirportTranfer, string Adults, string Children, string SpecialRequest, string RoomName, string Rooms, string LastCurrencyCode, string Source)
      {
        EngineLogDataSet.RequestWebSiteRow requestWebSiteRow = (EngineLogDataSet.RequestWebSiteRow) this.NewRow();
        object[] objArray = new object[31]
        {
          (object) LastName,
          (object) FirstName,
          (object) Telephone,
          (object) Adress,
          (object) City,
          (object) Estate,
          (object) Country,
          (object) PostalCode,
          (object) Email,
          (object) ConfirmationNumber,
          (object) CheckIn,
          (object) CheckOut,
          (object) SetupAgent,
          (object) ReservationStatus,
          (object) Group,
          (object) Hotel,
          (object) Nights,
          (object) Total,
          (object) Rate,
          (object) Balance,
          (object) CurrencyCode,
          (object) LastNight,
          (object) FreeNight,
          (object) AirportTranfer,
          (object) Adults,
          (object) Children,
          (object) SpecialRequest,
          (object) RoomName,
          (object) Rooms,
          (object) LastCurrencyCode,
          (object) Source
        };
        requestWebSiteRow.ItemArray = objArray;
        this.Rows.Add((DataRow) requestWebSiteRow);
        return requestWebSiteRow;
      }

      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        EngineLogDataSet.RequestWebSiteDataTable webSiteDataTable = (EngineLogDataSet.RequestWebSiteDataTable) base.Clone();
        webSiteDataTable.InitVars();
        return (DataTable) webSiteDataTable;
      }

      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new EngineLogDataSet.RequestWebSiteDataTable();
      }

      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnLastName = this.Columns["LastName"];
        this.columnFirstName = this.Columns["FirstName"];
        this.columnTelephone = this.Columns["Telephone"];
        this.columnAdress = this.Columns["Adress"];
        this.columnCity = this.Columns["City"];
        this.columnEstate = this.Columns["Estate"];
        this.columnCountry = this.Columns["Country"];
        this.columnPostalCode = this.Columns["PostalCode"];
        this.columnEmail = this.Columns["Email"];
        this.columnConfirmationNumber = this.Columns["ConfirmationNumber"];
        this.columnCheckIn = this.Columns["CheckIn"];
        this.columnCheckOut = this.Columns["CheckOut"];
        this.columnSetupAgent = this.Columns["SetupAgent"];
        this.columnReservationStatus = this.Columns["ReservationStatus"];
        this.columnGroup = this.Columns["Group"];
        this.columnHotel = this.Columns["Hotel"];
        this.columnNights = this.Columns["Nights"];
        this.columnTotal = this.Columns["Total"];
        this.columnRate = this.Columns["Rate"];
        this.columnBalance = this.Columns["Balance"];
        this.columnCurrencyCode = this.Columns["CurrencyCode"];
        this.columnLastNight = this.Columns["LastNight"];
        this.columnFreeNight = this.Columns["FreeNight"];
        this.columnAirportTranfer = this.Columns["AirportTranfer"];
        this.columnAdults = this.Columns["Adults"];
        this.columnChildren = this.Columns["Children"];
        this.columnSpecialRequest = this.Columns["SpecialRequest"];
        this.columnRoomName = this.Columns["RoomName"];
        this.columnRooms = this.Columns["Rooms"];
        this.columnLastCurrencyCode = this.Columns["LastCurrencyCode"];
        this.columnSource = this.Columns["Source"];
      }

      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnLastName = new DataColumn("LastName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastName);
        this.columnFirstName = new DataColumn("FirstName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFirstName);
        this.columnTelephone = new DataColumn("Telephone", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTelephone);
        this.columnAdress = new DataColumn("Adress", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAdress);
        this.columnCity = new DataColumn("City", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCity);
        this.columnEstate = new DataColumn("Estate", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnEstate);
        this.columnCountry = new DataColumn("Country", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCountry);
        this.columnPostalCode = new DataColumn("PostalCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnPostalCode);
        this.columnEmail = new DataColumn("Email", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnEmail);
        this.columnConfirmationNumber = new DataColumn("ConfirmationNumber", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnConfirmationNumber);
        this.columnCheckIn = new DataColumn("CheckIn", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCheckIn);
        this.columnCheckOut = new DataColumn("CheckOut", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCheckOut);
        this.columnSetupAgent = new DataColumn("SetupAgent", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSetupAgent);
        this.columnReservationStatus = new DataColumn("ReservationStatus", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationStatus);
        this.columnGroup = new DataColumn("Group", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnGroup);
        this.columnHotel = new DataColumn("Hotel", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnHotel);
        this.columnNights = new DataColumn("Nights", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNights);
        this.columnTotal = new DataColumn("Total", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTotal);
        this.columnRate = new DataColumn("Rate", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRate);
        this.columnBalance = new DataColumn("Balance", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnBalance);
        this.columnCurrencyCode = new DataColumn("CurrencyCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCurrencyCode);
        this.columnLastNight = new DataColumn("LastNight", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastNight);
        this.columnFreeNight = new DataColumn("FreeNight", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFreeNight);
        this.columnAirportTranfer = new DataColumn("AirportTranfer", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAirportTranfer);
        this.columnAdults = new DataColumn("Adults", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAdults);
        this.columnChildren = new DataColumn("Children", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnChildren);
        this.columnSpecialRequest = new DataColumn("SpecialRequest", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSpecialRequest);
        this.columnRoomName = new DataColumn("RoomName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRoomName);
        this.columnRooms = new DataColumn("Rooms", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRooms);
        this.columnLastCurrencyCode = new DataColumn("LastCurrencyCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastCurrencyCode);
        this.columnSource = new DataColumn("Source", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnSource);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestWebSiteRow NewRequestWebSiteRow()
      {
        return (EngineLogDataSet.RequestWebSiteRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new EngineLogDataSet.RequestWebSiteRow(builder);
      }

      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (EngineLogDataSet.RequestWebSiteRow);
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.RequestWebSiteRowChanged == null)
          return;
        this.RequestWebSiteRowChanged((object) this, new EngineLogDataSet.RequestWebSiteRowChangeEvent((EngineLogDataSet.RequestWebSiteRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.RequestWebSiteRowChanging == null)
          return;
        this.RequestWebSiteRowChanging((object) this, new EngineLogDataSet.RequestWebSiteRowChangeEvent((EngineLogDataSet.RequestWebSiteRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.RequestWebSiteRowDeleted == null)
          return;
        this.RequestWebSiteRowDeleted((object) this, new EngineLogDataSet.RequestWebSiteRowChangeEvent((EngineLogDataSet.RequestWebSiteRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.RequestWebSiteRowDeleting == null)
          return;
        this.RequestWebSiteRowDeleting((object) this, new EngineLogDataSet.RequestWebSiteRowChangeEvent((EngineLogDataSet.RequestWebSiteRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      public void RemoveRequestWebSiteRow(EngineLogDataSet.RequestWebSiteRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = engineLogDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (RequestWebSiteDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [XmlSchemaProvider("GetTypedTableSchema")]
    [Serializable]
    public class RequestAgencyDataTable : TypedTableBase<EngineLogDataSet.RequestAgencyRow>
    {
      private DataColumn columnLastName;
      private DataColumn columnFirstName;
      private DataColumn columnAgencyCode;
      private DataColumn columnAgentName;
      private DataColumn columnConfirmationNumber;
      private DataColumn columnReservationDate;
      private DataColumn columnCheckIn;
      private DataColumn columnCheckOut;
      private DataColumn columnReservationStatus;
      private DataColumn columnHotel;
      private DataColumn columnNights;
      private DataColumn columnTotal;
      private DataColumn columnRate;
      private DataColumn columnCurrency;
      private DataColumn columnAdults;
      private DataColumn columnChildrens;
      private DataColumn columnRoomName;
      private DataColumn columnRooms;

      [DebuggerNonUserCode]
      public RequestAgencyDataTable()
      {
        this.TableName = "RequestAgency";
        this.BeginInit();
        this.InitClass();
        this.EndInit();
      }

      [DebuggerNonUserCode]
      internal RequestAgencyDataTable(DataTable table)
      {
        this.TableName = table.TableName;
        if (table.CaseSensitive != table.DataSet.CaseSensitive)
          this.CaseSensitive = table.CaseSensitive;
        if (table.Locale.ToString() != table.DataSet.Locale.ToString())
          this.Locale = table.Locale;
        if (table.Namespace != table.DataSet.Namespace)
          this.Namespace = table.Namespace;
        this.Prefix = table.Prefix;
        this.MinimumCapacity = table.MinimumCapacity;
      }

      [DebuggerNonUserCode]
      protected RequestAgencyDataTable(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
        this.InitVars();
      }

      [DebuggerNonUserCode]
      public DataColumn LastNameColumn
      {
        get
        {
          return this.columnLastName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn FirstNameColumn
      {
        get
        {
          return this.columnFirstName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AgencyCodeColumn
      {
        get
        {
          return this.columnAgencyCode;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AgentNameColumn
      {
        get
        {
          return this.columnAgentName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ConfirmationNumberColumn
      {
        get
        {
          return this.columnConfirmationNumber;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationDateColumn
      {
        get
        {
          return this.columnReservationDate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CheckInColumn
      {
        get
        {
          return this.columnCheckIn;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CheckOutColumn
      {
        get
        {
          return this.columnCheckOut;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ReservationStatusColumn
      {
        get
        {
          return this.columnReservationStatus;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn HotelColumn
      {
        get
        {
          return this.columnHotel;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn NightsColumn
      {
        get
        {
          return this.columnNights;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn TotalColumn
      {
        get
        {
          return this.columnTotal;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RateColumn
      {
        get
        {
          return this.columnRate;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn CurrencyColumn
      {
        get
        {
          return this.columnCurrency;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn AdultsColumn
      {
        get
        {
          return this.columnAdults;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn ChildrensColumn
      {
        get
        {
          return this.columnChildrens;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RoomNameColumn
      {
        get
        {
          return this.columnRoomName;
        }
      }

      [DebuggerNonUserCode]
      public DataColumn RoomsColumn
      {
        get
        {
          return this.columnRooms;
        }
      }

      [DebuggerNonUserCode]
      [Browsable(false)]
      public int Count
      {
        get
        {
          return this.Rows.Count;
        }
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestAgencyRow this[int index]
      {
        get
        {
          return (EngineLogDataSet.RequestAgencyRow) this.Rows[index];
        }
      }

      public event EngineLogDataSet.RequestAgencyRowChangeEventHandler RequestAgencyRowChanging;

      public event EngineLogDataSet.RequestAgencyRowChangeEventHandler RequestAgencyRowChanged;

      public event EngineLogDataSet.RequestAgencyRowChangeEventHandler RequestAgencyRowDeleting;

      public event EngineLogDataSet.RequestAgencyRowChangeEventHandler RequestAgencyRowDeleted;

      [DebuggerNonUserCode]
      public void AddRequestAgencyRow(EngineLogDataSet.RequestAgencyRow row)
      {
        this.Rows.Add((DataRow) row);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestAgencyRow AddRequestAgencyRow(string LastName, string FirstName, string AgencyCode, string AgentName, string ConfirmationNumber, string ReservationDate, string CheckIn, string CheckOut, string ReservationStatus, string Hotel, string Nights, string Total, string Rate, string Currency, string Adults, string Childrens, string RoomName, string Rooms)
      {
        EngineLogDataSet.RequestAgencyRow requestAgencyRow = (EngineLogDataSet.RequestAgencyRow) this.NewRow();
        object[] objArray = new object[18]
        {
          (object) LastName,
          (object) FirstName,
          (object) AgencyCode,
          (object) AgentName,
          (object) ConfirmationNumber,
          (object) ReservationDate,
          (object) CheckIn,
          (object) CheckOut,
          (object) ReservationStatus,
          (object) Hotel,
          (object) Nights,
          (object) Total,
          (object) Rate,
          (object) Currency,
          (object) Adults,
          (object) Childrens,
          (object) RoomName,
          (object) Rooms
        };
        requestAgencyRow.ItemArray = objArray;
        this.Rows.Add((DataRow) requestAgencyRow);
        return requestAgencyRow;
      }

      [DebuggerNonUserCode]
      public override DataTable Clone()
      {
        EngineLogDataSet.RequestAgencyDataTable requestAgencyDataTable = (EngineLogDataSet.RequestAgencyDataTable) base.Clone();
        requestAgencyDataTable.InitVars();
        return (DataTable) requestAgencyDataTable;
      }

      [DebuggerNonUserCode]
      protected override DataTable CreateInstance()
      {
        return (DataTable) new EngineLogDataSet.RequestAgencyDataTable();
      }

      [DebuggerNonUserCode]
      internal void InitVars()
      {
        this.columnLastName = this.Columns["LastName"];
        this.columnFirstName = this.Columns["FirstName"];
        this.columnAgencyCode = this.Columns["AgencyCode"];
        this.columnAgentName = this.Columns["AgentName"];
        this.columnConfirmationNumber = this.Columns["ConfirmationNumber"];
        this.columnReservationDate = this.Columns["ReservationDate"];
        this.columnCheckIn = this.Columns["CheckIn"];
        this.columnCheckOut = this.Columns["CheckOut"];
        this.columnReservationStatus = this.Columns["ReservationStatus"];
        this.columnHotel = this.Columns["Hotel"];
        this.columnNights = this.Columns["Nights"];
        this.columnTotal = this.Columns["Total"];
        this.columnRate = this.Columns["Rate"];
        this.columnCurrency = this.Columns["Currency"];
        this.columnAdults = this.Columns["Adults"];
        this.columnChildrens = this.Columns["Childrens"];
        this.columnRoomName = this.Columns["RoomName"];
        this.columnRooms = this.Columns["Rooms"];
      }

      [DebuggerNonUserCode]
      private void InitClass()
      {
        this.columnLastName = new DataColumn("LastName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnLastName);
        this.columnFirstName = new DataColumn("FirstName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnFirstName);
        this.columnAgencyCode = new DataColumn("AgencyCode", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAgencyCode);
        this.columnAgentName = new DataColumn("AgentName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAgentName);
        this.columnConfirmationNumber = new DataColumn("ConfirmationNumber", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnConfirmationNumber);
        this.columnReservationDate = new DataColumn("ReservationDate", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationDate);
        this.columnCheckIn = new DataColumn("CheckIn", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCheckIn);
        this.columnCheckOut = new DataColumn("CheckOut", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCheckOut);
        this.columnReservationStatus = new DataColumn("ReservationStatus", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnReservationStatus);
        this.columnHotel = new DataColumn("Hotel", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnHotel);
        this.columnNights = new DataColumn("Nights", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnNights);
        this.columnTotal = new DataColumn("Total", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnTotal);
        this.columnRate = new DataColumn("Rate", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRate);
        this.columnCurrency = new DataColumn("Currency", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnCurrency);
        this.columnAdults = new DataColumn("Adults", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnAdults);
        this.columnChildrens = new DataColumn("Childrens", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnChildrens);
        this.columnRoomName = new DataColumn("RoomName", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRoomName);
        this.columnRooms = new DataColumn("Rooms", typeof (string), (string) null, MappingType.Element);
        this.Columns.Add(this.columnRooms);
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestAgencyRow NewRequestAgencyRow()
      {
        return (EngineLogDataSet.RequestAgencyRow) this.NewRow();
      }

      [DebuggerNonUserCode]
      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return (DataRow) new EngineLogDataSet.RequestAgencyRow(builder);
      }

      [DebuggerNonUserCode]
      protected override Type GetRowType()
      {
        return typeof (EngineLogDataSet.RequestAgencyRow);
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanged(DataRowChangeEventArgs e)
      {
        base.OnRowChanged(e);
        if (this.RequestAgencyRowChanged == null)
          return;
        this.RequestAgencyRowChanged((object) this, new EngineLogDataSet.RequestAgencyRowChangeEvent((EngineLogDataSet.RequestAgencyRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowChanging(DataRowChangeEventArgs e)
      {
        base.OnRowChanging(e);
        if (this.RequestAgencyRowChanging == null)
          return;
        this.RequestAgencyRowChanging((object) this, new EngineLogDataSet.RequestAgencyRowChangeEvent((EngineLogDataSet.RequestAgencyRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleted(DataRowChangeEventArgs e)
      {
        base.OnRowDeleted(e);
        if (this.RequestAgencyRowDeleted == null)
          return;
        this.RequestAgencyRowDeleted((object) this, new EngineLogDataSet.RequestAgencyRowChangeEvent((EngineLogDataSet.RequestAgencyRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      protected override void OnRowDeleting(DataRowChangeEventArgs e)
      {
        base.OnRowDeleting(e);
        if (this.RequestAgencyRowDeleting == null)
          return;
        this.RequestAgencyRowDeleting((object) this, new EngineLogDataSet.RequestAgencyRowChangeEvent((EngineLogDataSet.RequestAgencyRow) e.Row, e.Action));
      }

      [DebuggerNonUserCode]
      public void RemoveRequestAgencyRow(EngineLogDataSet.RequestAgencyRow row)
      {
        this.Rows.Remove((DataRow) row);
      }

      [DebuggerNonUserCode]
      public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
      {
        XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
        XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
        EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
        XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
        xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
        xmlSchemaAny1.MinOccurs = new Decimal(0);
        xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
        xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
        XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
        xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
        xmlSchemaAny2.MinOccurs = new Decimal(1);
        xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
        xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "namespace",
          FixedValue = engineLogDataSet.Namespace
        });
        schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
        {
          Name = "tableTypeName",
          FixedValue = nameof (RequestAgencyDataTable)
        });
        schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
        XmlSchema schemaSerializable = engineLogDataSet.GetSchemaSerializable();
        if (xs.Contains(schemaSerializable.TargetNamespace))
        {
          MemoryStream memoryStream1 = new MemoryStream();
          MemoryStream memoryStream2 = new MemoryStream();
          try
          {
            schemaSerializable.Write((Stream) memoryStream1);
            foreach (XmlSchema schema in (IEnumerable) xs.Schemas(schemaSerializable.TargetNamespace))
            {
              memoryStream2.SetLength(0L);
              schema.Write((Stream) memoryStream2);
              if (memoryStream1.Length == memoryStream2.Length)
              {
                memoryStream1.Position = 0L;
                memoryStream2.Position = 0L;
                do
                  ;
                while (memoryStream1.Position != memoryStream1.Length && memoryStream1.ReadByte() == memoryStream2.ReadByte());
                if (memoryStream1.Position == memoryStream1.Length)
                  return schemaComplexType;
              }
            }
          }
          finally
          {
            if (memoryStream1 != null)
              memoryStream1.Close();
            if (memoryStream2 != null)
              memoryStream2.Close();
          }
        }
        xs.Add(schemaSerializable);
        return schemaComplexType;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class OriginalBookingRow : DataRow
    {
      private EngineLogDataSet.OriginalBookingDataTable tableOriginalBooking;

      [DebuggerNonUserCode]
      internal OriginalBookingRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableOriginalBooking = (EngineLogDataSet.OriginalBookingDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      public int OrginalBookingID
      {
        get
        {
          return (int) this[this.tableOriginalBooking.OrginalBookingIDColumn];
        }
        set
        {
          this[this.tableOriginalBooking.OrginalBookingIDColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string FileName
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.FileNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FileName' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.FileNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string FileContent
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.FileContentColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FileContent' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.FileContentColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Url
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.UrlColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Url' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.UrlColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public int Status
      {
        get
        {
          try
          {
            return (int) this[this.tableOriginalBooking.StatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Status' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.StatusColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ID
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.IDColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ID' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.IDColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public DateTime Date
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableOriginalBooking.DateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Date' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.DateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Type
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.TypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Type' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.TypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationNumberOrginal
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.ReservationNumberOrginalColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationNumberOrginal' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.ReservationNumberOrginalColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationNumberUv
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.ReservationNumberUvColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationNumberUv' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.ReservationNumberUvColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string StatusText
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.StatusTextColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'StatusText' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.StatusTextColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public int HotelIdUv
      {
        get
        {
          try
          {
            return (int) this[this.tableOriginalBooking.HotelIdUvColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'HotelIdUv' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.HotelIdUvColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string HotelName
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.HotelNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'HotelName' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.HotelNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public int ReservationType
      {
        get
        {
          try
          {
            return (int) this[this.tableOriginalBooking.ReservationTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationType' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.ReservationTypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AgencyCode
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.AgencyCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AgencyCode' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.AgencyCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AgencyName
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.AgencyNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AgencyName' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.AgencyNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AgentName
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.AgentNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AgentName' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.AgentNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string SetupAgent
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.SetupAgentColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'SetupAgent' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.SetupAgentColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationTypeCode
      {
        get
        {
          try
          {
            return (string) this[this.tableOriginalBooking.ReservationTypeCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationTypeCode' de la tabla 'OriginalBooking' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBooking.ReservationTypeCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public bool IsFileNameNull()
      {
        return this.IsNull(this.tableOriginalBooking.FileNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetFileNameNull()
      {
        this[this.tableOriginalBooking.FileNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsFileContentNull()
      {
        return this.IsNull(this.tableOriginalBooking.FileContentColumn);
      }

      [DebuggerNonUserCode]
      public void SetFileContentNull()
      {
        this[this.tableOriginalBooking.FileContentColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsUrlNull()
      {
        return this.IsNull(this.tableOriginalBooking.UrlColumn);
      }

      [DebuggerNonUserCode]
      public void SetUrlNull()
      {
        this[this.tableOriginalBooking.UrlColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsStatusNull()
      {
        return this.IsNull(this.tableOriginalBooking.StatusColumn);
      }

      [DebuggerNonUserCode]
      public void SetStatusNull()
      {
        this[this.tableOriginalBooking.StatusColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsIDNull()
      {
        return this.IsNull(this.tableOriginalBooking.IDColumn);
      }

      [DebuggerNonUserCode]
      public void SetIDNull()
      {
        this[this.tableOriginalBooking.IDColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsDateNull()
      {
        return this.IsNull(this.tableOriginalBooking.DateColumn);
      }

      [DebuggerNonUserCode]
      public void SetDateNull()
      {
        this[this.tableOriginalBooking.DateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsTypeNull()
      {
        return this.IsNull(this.tableOriginalBooking.TypeColumn);
      }

      [DebuggerNonUserCode]
      public void SetTypeNull()
      {
        this[this.tableOriginalBooking.TypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationNumberOrginalNull()
      {
        return this.IsNull(this.tableOriginalBooking.ReservationNumberOrginalColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationNumberOrginalNull()
      {
        this[this.tableOriginalBooking.ReservationNumberOrginalColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationNumberUvNull()
      {
        return this.IsNull(this.tableOriginalBooking.ReservationNumberUvColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationNumberUvNull()
      {
        this[this.tableOriginalBooking.ReservationNumberUvColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsStatusTextNull()
      {
        return this.IsNull(this.tableOriginalBooking.StatusTextColumn);
      }

      [DebuggerNonUserCode]
      public void SetStatusTextNull()
      {
        this[this.tableOriginalBooking.StatusTextColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsHotelIdUvNull()
      {
        return this.IsNull(this.tableOriginalBooking.HotelIdUvColumn);
      }

      [DebuggerNonUserCode]
      public void SetHotelIdUvNull()
      {
        this[this.tableOriginalBooking.HotelIdUvColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsHotelNameNull()
      {
        return this.IsNull(this.tableOriginalBooking.HotelNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetHotelNameNull()
      {
        this[this.tableOriginalBooking.HotelNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationTypeNull()
      {
        return this.IsNull(this.tableOriginalBooking.ReservationTypeColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationTypeNull()
      {
        this[this.tableOriginalBooking.ReservationTypeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAgencyCodeNull()
      {
        return this.IsNull(this.tableOriginalBooking.AgencyCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetAgencyCodeNull()
      {
        this[this.tableOriginalBooking.AgencyCodeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAgencyNameNull()
      {
        return this.IsNull(this.tableOriginalBooking.AgencyNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetAgencyNameNull()
      {
        this[this.tableOriginalBooking.AgencyNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAgentNameNull()
      {
        return this.IsNull(this.tableOriginalBooking.AgentNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetAgentNameNull()
      {
        this[this.tableOriginalBooking.AgentNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsSetupAgentNull()
      {
        return this.IsNull(this.tableOriginalBooking.SetupAgentColumn);
      }

      [DebuggerNonUserCode]
      public void SetSetupAgentNull()
      {
        this[this.tableOriginalBooking.SetupAgentColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationTypeCodeNull()
      {
        return this.IsNull(this.tableOriginalBooking.ReservationTypeCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationTypeCodeNull()
      {
        this[this.tableOriginalBooking.ReservationTypeCodeColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class OriginalBookingExistsForFileNameRow : DataRow
    {
      private EngineLogDataSet.OriginalBookingExistsForFileNameDataTable tableOriginalBookingExistsForFileName;

      [DebuggerNonUserCode]
      internal OriginalBookingExistsForFileNameRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableOriginalBookingExistsForFileName = (EngineLogDataSet.OriginalBookingExistsForFileNameDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      public int Count
      {
        get
        {
          try
          {
            return (int) this[this.tableOriginalBookingExistsForFileName.CountColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Count' de la tabla 'OriginalBookingExistsForFileName' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableOriginalBookingExistsForFileName.CountColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public bool IsCountNull()
      {
        return this.IsNull(this.tableOriginalBookingExistsForFileName.CountColumn);
      }

      [DebuggerNonUserCode]
      public void SetCountNull()
      {
        this[this.tableOriginalBookingExistsForFileName.CountColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class EngineLogRow : DataRow
    {
      private EngineLogDataSet.EngineLogDataTable tableEngineLog;

      [DebuggerNonUserCode]
      internal EngineLogRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableEngineLog = (EngineLogDataSet.EngineLogDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      public string FileName
      {
        get
        {
          try
          {
            return (string) this[this.tableEngineLog.FileNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FileName' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.FileNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Content
      {
        get
        {
          try
          {
            return (string) this[this.tableEngineLog.ContentColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Content' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.ContentColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Url
      {
        get
        {
          try
          {
            return (string) this[this.tableEngineLog.UrlColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Url' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.UrlColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public int Status
      {
        get
        {
          try
          {
            return (int) this[this.tableEngineLog.StatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Status' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.StatusColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ID
      {
        get
        {
          try
          {
            return (string) this[this.tableEngineLog.IDColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ID' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.IDColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public DateTime Date
      {
        get
        {
          try
          {
            return (DateTime) this[this.tableEngineLog.DateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Date' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.DateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public int ReservationType
      {
        get
        {
          try
          {
            return (int) this[this.tableEngineLog.ReservationTypeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationType' de la tabla 'EngineLog' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableEngineLog.ReservationTypeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public bool IsFileNameNull()
      {
        return this.IsNull(this.tableEngineLog.FileNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetFileNameNull()
      {
        this[this.tableEngineLog.FileNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsContentNull()
      {
        return this.IsNull(this.tableEngineLog.ContentColumn);
      }

      [DebuggerNonUserCode]
      public void SetContentNull()
      {
        this[this.tableEngineLog.ContentColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsUrlNull()
      {
        return this.IsNull(this.tableEngineLog.UrlColumn);
      }

      [DebuggerNonUserCode]
      public void SetUrlNull()
      {
        this[this.tableEngineLog.UrlColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsStatusNull()
      {
        return this.IsNull(this.tableEngineLog.StatusColumn);
      }

      [DebuggerNonUserCode]
      public void SetStatusNull()
      {
        this[this.tableEngineLog.StatusColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsIDNull()
      {
        return this.IsNull(this.tableEngineLog.IDColumn);
      }

      [DebuggerNonUserCode]
      public void SetIDNull()
      {
        this[this.tableEngineLog.IDColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsDateNull()
      {
        return this.IsNull(this.tableEngineLog.DateColumn);
      }

      [DebuggerNonUserCode]
      public void SetDateNull()
      {
        this[this.tableEngineLog.DateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationTypeNull()
      {
        return this.IsNull(this.tableEngineLog.ReservationTypeColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationTypeNull()
      {
        this[this.tableEngineLog.ReservationTypeColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class RequestWebSiteRow : DataRow
    {
      private EngineLogDataSet.RequestWebSiteDataTable tableRequestWebSite;

      [DebuggerNonUserCode]
      internal RequestWebSiteRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableRequestWebSite = (EngineLogDataSet.RequestWebSiteDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      public string LastName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.LastNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'LastName' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.LastNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string FirstName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.FirstNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FirstName' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.FirstNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Telephone
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.TelephoneColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Telephone' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.TelephoneColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Adress
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.AdressColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Adress' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.AdressColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string City
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.CityColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'City' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.CityColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Estate
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.EstateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Estate' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.EstateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Country
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.CountryColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Country' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.CountryColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string PostalCode
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.PostalCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'PostalCode' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.PostalCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Email
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.EmailColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Email' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.EmailColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ConfirmationNumber
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.ConfirmationNumberColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ConfirmationNumber' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.ConfirmationNumberColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string CheckIn
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.CheckInColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'CheckIn' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.CheckInColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string CheckOut
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.CheckOutColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'CheckOut' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.CheckOutColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string SetupAgent
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.SetupAgentColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'SetupAgent' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.SetupAgentColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationStatus
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.ReservationStatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationStatus' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.ReservationStatusColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Group
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.GroupColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Group' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.GroupColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Hotel
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.HotelColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Hotel' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.HotelColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Nights
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.NightsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Nights' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.NightsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Total
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.TotalColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Total' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.TotalColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Rate
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.RateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Rate' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.RateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Balance
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.BalanceColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Balance' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.BalanceColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string CurrencyCode
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.CurrencyCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'CurrencyCode' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.CurrencyCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string LastNight
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.LastNightColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'LastNight' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.LastNightColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string FreeNight
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.FreeNightColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FreeNight' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.FreeNightColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AirportTranfer
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.AirportTranferColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AirportTranfer' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.AirportTranferColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Adults
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.AdultsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Adults' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.AdultsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Children
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.ChildrenColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Children' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.ChildrenColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string SpecialRequest
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.SpecialRequestColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'SpecialRequest' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.SpecialRequestColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string RoomName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.RoomNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'RoomName' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.RoomNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Rooms
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.RoomsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Rooms' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.RoomsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string LastCurrencyCode
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.LastCurrencyCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'LastCurrencyCode' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.LastCurrencyCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Source
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestWebSite.SourceColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Source' de la tabla 'RequestWebSite' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestWebSite.SourceColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public bool IsLastNameNull()
      {
        return this.IsNull(this.tableRequestWebSite.LastNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetLastNameNull()
      {
        this[this.tableRequestWebSite.LastNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsFirstNameNull()
      {
        return this.IsNull(this.tableRequestWebSite.FirstNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetFirstNameNull()
      {
        this[this.tableRequestWebSite.FirstNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsTelephoneNull()
      {
        return this.IsNull(this.tableRequestWebSite.TelephoneColumn);
      }

      [DebuggerNonUserCode]
      public void SetTelephoneNull()
      {
        this[this.tableRequestWebSite.TelephoneColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAdressNull()
      {
        return this.IsNull(this.tableRequestWebSite.AdressColumn);
      }

      [DebuggerNonUserCode]
      public void SetAdressNull()
      {
        this[this.tableRequestWebSite.AdressColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCityNull()
      {
        return this.IsNull(this.tableRequestWebSite.CityColumn);
      }

      [DebuggerNonUserCode]
      public void SetCityNull()
      {
        this[this.tableRequestWebSite.CityColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsEstateNull()
      {
        return this.IsNull(this.tableRequestWebSite.EstateColumn);
      }

      [DebuggerNonUserCode]
      public void SetEstateNull()
      {
        this[this.tableRequestWebSite.EstateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCountryNull()
      {
        return this.IsNull(this.tableRequestWebSite.CountryColumn);
      }

      [DebuggerNonUserCode]
      public void SetCountryNull()
      {
        this[this.tableRequestWebSite.CountryColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsPostalCodeNull()
      {
        return this.IsNull(this.tableRequestWebSite.PostalCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetPostalCodeNull()
      {
        this[this.tableRequestWebSite.PostalCodeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsEmailNull()
      {
        return this.IsNull(this.tableRequestWebSite.EmailColumn);
      }

      [DebuggerNonUserCode]
      public void SetEmailNull()
      {
        this[this.tableRequestWebSite.EmailColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsConfirmationNumberNull()
      {
        return this.IsNull(this.tableRequestWebSite.ConfirmationNumberColumn);
      }

      [DebuggerNonUserCode]
      public void SetConfirmationNumberNull()
      {
        this[this.tableRequestWebSite.ConfirmationNumberColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCheckInNull()
      {
        return this.IsNull(this.tableRequestWebSite.CheckInColumn);
      }

      [DebuggerNonUserCode]
      public void SetCheckInNull()
      {
        this[this.tableRequestWebSite.CheckInColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCheckOutNull()
      {
        return this.IsNull(this.tableRequestWebSite.CheckOutColumn);
      }

      [DebuggerNonUserCode]
      public void SetCheckOutNull()
      {
        this[this.tableRequestWebSite.CheckOutColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsSetupAgentNull()
      {
        return this.IsNull(this.tableRequestWebSite.SetupAgentColumn);
      }

      [DebuggerNonUserCode]
      public void SetSetupAgentNull()
      {
        this[this.tableRequestWebSite.SetupAgentColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationStatusNull()
      {
        return this.IsNull(this.tableRequestWebSite.ReservationStatusColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationStatusNull()
      {
        this[this.tableRequestWebSite.ReservationStatusColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsGroupNull()
      {
        return this.IsNull(this.tableRequestWebSite.GroupColumn);
      }

      [DebuggerNonUserCode]
      public void SetGroupNull()
      {
        this[this.tableRequestWebSite.GroupColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsHotelNull()
      {
        return this.IsNull(this.tableRequestWebSite.HotelColumn);
      }

      [DebuggerNonUserCode]
      public void SetHotelNull()
      {
        this[this.tableRequestWebSite.HotelColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsNightsNull()
      {
        return this.IsNull(this.tableRequestWebSite.NightsColumn);
      }

      [DebuggerNonUserCode]
      public void SetNightsNull()
      {
        this[this.tableRequestWebSite.NightsColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsTotalNull()
      {
        return this.IsNull(this.tableRequestWebSite.TotalColumn);
      }

      [DebuggerNonUserCode]
      public void SetTotalNull()
      {
        this[this.tableRequestWebSite.TotalColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRateNull()
      {
        return this.IsNull(this.tableRequestWebSite.RateColumn);
      }

      [DebuggerNonUserCode]
      public void SetRateNull()
      {
        this[this.tableRequestWebSite.RateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsBalanceNull()
      {
        return this.IsNull(this.tableRequestWebSite.BalanceColumn);
      }

      [DebuggerNonUserCode]
      public void SetBalanceNull()
      {
        this[this.tableRequestWebSite.BalanceColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCurrencyCodeNull()
      {
        return this.IsNull(this.tableRequestWebSite.CurrencyCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetCurrencyCodeNull()
      {
        this[this.tableRequestWebSite.CurrencyCodeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsLastNightNull()
      {
        return this.IsNull(this.tableRequestWebSite.LastNightColumn);
      }

      [DebuggerNonUserCode]
      public void SetLastNightNull()
      {
        this[this.tableRequestWebSite.LastNightColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsFreeNightNull()
      {
        return this.IsNull(this.tableRequestWebSite.FreeNightColumn);
      }

      [DebuggerNonUserCode]
      public void SetFreeNightNull()
      {
        this[this.tableRequestWebSite.FreeNightColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAirportTranferNull()
      {
        return this.IsNull(this.tableRequestWebSite.AirportTranferColumn);
      }

      [DebuggerNonUserCode]
      public void SetAirportTranferNull()
      {
        this[this.tableRequestWebSite.AirportTranferColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAdultsNull()
      {
        return this.IsNull(this.tableRequestWebSite.AdultsColumn);
      }

      [DebuggerNonUserCode]
      public void SetAdultsNull()
      {
        this[this.tableRequestWebSite.AdultsColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsChildrenNull()
      {
        return this.IsNull(this.tableRequestWebSite.ChildrenColumn);
      }

      [DebuggerNonUserCode]
      public void SetChildrenNull()
      {
        this[this.tableRequestWebSite.ChildrenColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsSpecialRequestNull()
      {
        return this.IsNull(this.tableRequestWebSite.SpecialRequestColumn);
      }

      [DebuggerNonUserCode]
      public void SetSpecialRequestNull()
      {
        this[this.tableRequestWebSite.SpecialRequestColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRoomNameNull()
      {
        return this.IsNull(this.tableRequestWebSite.RoomNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetRoomNameNull()
      {
        this[this.tableRequestWebSite.RoomNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRoomsNull()
      {
        return this.IsNull(this.tableRequestWebSite.RoomsColumn);
      }

      [DebuggerNonUserCode]
      public void SetRoomsNull()
      {
        this[this.tableRequestWebSite.RoomsColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsLastCurrencyCodeNull()
      {
        return this.IsNull(this.tableRequestWebSite.LastCurrencyCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetLastCurrencyCodeNull()
      {
        this[this.tableRequestWebSite.LastCurrencyCodeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsSourceNull()
      {
        return this.IsNull(this.tableRequestWebSite.SourceColumn);
      }

      [DebuggerNonUserCode]
      public void SetSourceNull()
      {
        this[this.tableRequestWebSite.SourceColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class RequestAgencyRow : DataRow
    {
      private EngineLogDataSet.RequestAgencyDataTable tableRequestAgency;

      [DebuggerNonUserCode]
      internal RequestAgencyRow(DataRowBuilder rb)
        : base(rb)
      {
        this.tableRequestAgency = (EngineLogDataSet.RequestAgencyDataTable) this.Table;
      }

      [DebuggerNonUserCode]
      public string LastName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.LastNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'LastName' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.LastNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string FirstName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.FirstNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'FirstName' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.FirstNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AgencyCode
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.AgencyCodeColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AgencyCode' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.AgencyCodeColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string AgentName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.AgentNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'AgentName' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.AgentNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ConfirmationNumber
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.ConfirmationNumberColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ConfirmationNumber' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.ConfirmationNumberColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationDate
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.ReservationDateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationDate' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.ReservationDateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string CheckIn
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.CheckInColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'CheckIn' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.CheckInColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string CheckOut
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.CheckOutColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'CheckOut' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.CheckOutColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string ReservationStatus
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.ReservationStatusColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'ReservationStatus' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.ReservationStatusColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Hotel
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.HotelColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Hotel' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.HotelColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Nights
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.NightsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Nights' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.NightsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Total
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.TotalColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Total' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.TotalColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Rate
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.RateColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Rate' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.RateColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Currency
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.CurrencyColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Currency' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.CurrencyColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Adults
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.AdultsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Adults' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.AdultsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Childrens
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.ChildrensColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Childrens' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.ChildrensColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string RoomName
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.RoomNameColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'RoomName' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.RoomNameColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public string Rooms
      {
        get
        {
          try
          {
            return (string) this[this.tableRequestAgency.RoomsColumn];
          }
          catch (InvalidCastException ex)
          {
            throw new StrongTypingException("El valor de la columna 'Rooms' de la tabla 'RequestAgency' es DBNull.", (Exception) ex);
          }
        }
        set
        {
          this[this.tableRequestAgency.RoomsColumn] = (object) value;
        }
      }

      [DebuggerNonUserCode]
      public bool IsLastNameNull()
      {
        return this.IsNull(this.tableRequestAgency.LastNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetLastNameNull()
      {
        this[this.tableRequestAgency.LastNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsFirstNameNull()
      {
        return this.IsNull(this.tableRequestAgency.FirstNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetFirstNameNull()
      {
        this[this.tableRequestAgency.FirstNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAgencyCodeNull()
      {
        return this.IsNull(this.tableRequestAgency.AgencyCodeColumn);
      }

      [DebuggerNonUserCode]
      public void SetAgencyCodeNull()
      {
        this[this.tableRequestAgency.AgencyCodeColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAgentNameNull()
      {
        return this.IsNull(this.tableRequestAgency.AgentNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetAgentNameNull()
      {
        this[this.tableRequestAgency.AgentNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsConfirmationNumberNull()
      {
        return this.IsNull(this.tableRequestAgency.ConfirmationNumberColumn);
      }

      [DebuggerNonUserCode]
      public void SetConfirmationNumberNull()
      {
        this[this.tableRequestAgency.ConfirmationNumberColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationDateNull()
      {
        return this.IsNull(this.tableRequestAgency.ReservationDateColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationDateNull()
      {
        this[this.tableRequestAgency.ReservationDateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCheckInNull()
      {
        return this.IsNull(this.tableRequestAgency.CheckInColumn);
      }

      [DebuggerNonUserCode]
      public void SetCheckInNull()
      {
        this[this.tableRequestAgency.CheckInColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCheckOutNull()
      {
        return this.IsNull(this.tableRequestAgency.CheckOutColumn);
      }

      [DebuggerNonUserCode]
      public void SetCheckOutNull()
      {
        this[this.tableRequestAgency.CheckOutColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsReservationStatusNull()
      {
        return this.IsNull(this.tableRequestAgency.ReservationStatusColumn);
      }

      [DebuggerNonUserCode]
      public void SetReservationStatusNull()
      {
        this[this.tableRequestAgency.ReservationStatusColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsHotelNull()
      {
        return this.IsNull(this.tableRequestAgency.HotelColumn);
      }

      [DebuggerNonUserCode]
      public void SetHotelNull()
      {
        this[this.tableRequestAgency.HotelColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsNightsNull()
      {
        return this.IsNull(this.tableRequestAgency.NightsColumn);
      }

      [DebuggerNonUserCode]
      public void SetNightsNull()
      {
        this[this.tableRequestAgency.NightsColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsTotalNull()
      {
        return this.IsNull(this.tableRequestAgency.TotalColumn);
      }

      [DebuggerNonUserCode]
      public void SetTotalNull()
      {
        this[this.tableRequestAgency.TotalColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRateNull()
      {
        return this.IsNull(this.tableRequestAgency.RateColumn);
      }

      [DebuggerNonUserCode]
      public void SetRateNull()
      {
        this[this.tableRequestAgency.RateColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsCurrencyNull()
      {
        return this.IsNull(this.tableRequestAgency.CurrencyColumn);
      }

      [DebuggerNonUserCode]
      public void SetCurrencyNull()
      {
        this[this.tableRequestAgency.CurrencyColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsAdultsNull()
      {
        return this.IsNull(this.tableRequestAgency.AdultsColumn);
      }

      [DebuggerNonUserCode]
      public void SetAdultsNull()
      {
        this[this.tableRequestAgency.AdultsColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsChildrensNull()
      {
        return this.IsNull(this.tableRequestAgency.ChildrensColumn);
      }

      [DebuggerNonUserCode]
      public void SetChildrensNull()
      {
        this[this.tableRequestAgency.ChildrensColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRoomNameNull()
      {
        return this.IsNull(this.tableRequestAgency.RoomNameColumn);
      }

      [DebuggerNonUserCode]
      public void SetRoomNameNull()
      {
        this[this.tableRequestAgency.RoomNameColumn] = Convert.DBNull;
      }

      [DebuggerNonUserCode]
      public bool IsRoomsNull()
      {
        return this.IsNull(this.tableRequestAgency.RoomsColumn);
      }

      [DebuggerNonUserCode]
      public void SetRoomsNull()
      {
        this[this.tableRequestAgency.RoomsColumn] = Convert.DBNull;
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class OriginalBookingRowChangeEvent : EventArgs
    {
      private EngineLogDataSet.OriginalBookingRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      public OriginalBookingRowChangeEvent(EngineLogDataSet.OriginalBookingRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class OriginalBookingExistsForFileNameRowChangeEvent : EventArgs
    {
      private EngineLogDataSet.OriginalBookingExistsForFileNameRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      public OriginalBookingExistsForFileNameRowChangeEvent(EngineLogDataSet.OriginalBookingExistsForFileNameRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.OriginalBookingExistsForFileNameRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class EngineLogRowChangeEvent : EventArgs
    {
      private EngineLogDataSet.EngineLogRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      public EngineLogRowChangeEvent(EngineLogDataSet.EngineLogRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.EngineLogRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class RequestWebSiteRowChangeEvent : EventArgs
    {
      private EngineLogDataSet.RequestWebSiteRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      public RequestWebSiteRowChangeEvent(EngineLogDataSet.RequestWebSiteRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestWebSiteRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }

    [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    public class RequestAgencyRowChangeEvent : EventArgs
    {
      private EngineLogDataSet.RequestAgencyRow eventRow;
      private DataRowAction eventAction;

      [DebuggerNonUserCode]
      public RequestAgencyRowChangeEvent(EngineLogDataSet.RequestAgencyRow row, DataRowAction action)
      {
        this.eventRow = row;
        this.eventAction = action;
      }

      [DebuggerNonUserCode]
      public EngineLogDataSet.RequestAgencyRow Row
      {
        get
        {
          return this.eventRow;
        }
      }

      [DebuggerNonUserCode]
      public DataRowAction Action
      {
        get
        {
          return this.eventAction;
        }
      }
    }
  }
}
