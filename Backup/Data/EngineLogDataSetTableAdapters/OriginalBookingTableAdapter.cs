﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.Data.EngineLogDataSetTableAdapters.OriginalBookingTableAdapter
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using OriginalBooking.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

namespace OriginalBooking.Data.EngineLogDataSetTableAdapters
{
  [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  [DesignerCategory("code")]
  [ToolboxItem(true)]
  [DataObject(true)]
  [Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
  [HelpKeyword("vs.data.TableAdapter")]
  public class OriginalBookingTableAdapter : Component
  {
    private SqlDataAdapter _adapter;
    private SqlConnection _connection;
    private SqlTransaction _transaction;
    private SqlCommand[] _commandCollection;
    private bool _clearBeforeFill;

    [DebuggerNonUserCode]
    public OriginalBookingTableAdapter()
    {
      this.ClearBeforeFill = true;
    }

    [DebuggerNonUserCode]
    protected internal SqlDataAdapter Adapter
    {
      get
      {
        if (this._adapter == null)
          this.InitAdapter();
        return this._adapter;
      }
    }

    [DebuggerNonUserCode]
    internal SqlConnection Connection
    {
      get
      {
        if (this._connection == null)
          this.InitConnection();
        return this._connection;
      }
      set
      {
        this._connection = value;
        if (this.Adapter.InsertCommand != null)
          this.Adapter.InsertCommand.Connection = value;
        if (this.Adapter.DeleteCommand != null)
          this.Adapter.DeleteCommand.Connection = value;
        if (this.Adapter.UpdateCommand != null)
          this.Adapter.UpdateCommand.Connection = value;
        for (int index = 0; index < this.CommandCollection.Length; ++index)
        {
          if (this.CommandCollection[index] != null)
            this.CommandCollection[index].Connection = value;
        }
      }
    }

    [DebuggerNonUserCode]
    internal SqlTransaction Transaction
    {
      get
      {
        return this._transaction;
      }
      set
      {
        this._transaction = value;
        for (int index = 0; index < this.CommandCollection.Length; ++index)
          this.CommandCollection[index].Transaction = this._transaction;
        if (this.Adapter != null && this.Adapter.DeleteCommand != null)
          this.Adapter.DeleteCommand.Transaction = this._transaction;
        if (this.Adapter != null && this.Adapter.InsertCommand != null)
          this.Adapter.InsertCommand.Transaction = this._transaction;
        if (this.Adapter == null || this.Adapter.UpdateCommand == null)
          return;
        this.Adapter.UpdateCommand.Transaction = this._transaction;
      }
    }

    [DebuggerNonUserCode]
    protected SqlCommand[] CommandCollection
    {
      get
      {
        if (this._commandCollection == null)
          this.InitCommandCollection();
        return this._commandCollection;
      }
    }

    [DebuggerNonUserCode]
    public bool ClearBeforeFill
    {
      get
      {
        return this._clearBeforeFill;
      }
      set
      {
        this._clearBeforeFill = value;
      }
    }

    [DebuggerNonUserCode]
    private void InitAdapter()
    {
      this._adapter = new SqlDataAdapter();
      this._adapter.TableMappings.Add((object) new DataTableMapping()
      {
        SourceTable = "Table",
        DataSetTable = "OriginalBooking",
        ColumnMappings = {
          {
            "OrginalBookingID",
            "OrginalBookingID"
          },
          {
            "FileName",
            "FileName"
          },
          {
            "FileContent",
            "FileContent"
          },
          {
            "Url",
            "Url"
          },
          {
            "Status",
            "Status"
          },
          {
            "ID",
            "ID"
          },
          {
            "Date",
            "Date"
          },
          {
            "Type",
            "Type"
          },
          {
            "ReservationNumberOrginal",
            "ReservationNumberOrginal"
          },
          {
            "ReservationNumberUv",
            "ReservationNumberUv"
          },
          {
            "StatusText",
            "StatusText"
          },
          {
            "HotelIdUv",
            "HotelIdUv"
          },
          {
            "HotelName",
            "HotelName"
          },
          {
            "ReservationType",
            "ReservationType"
          },
          {
            "AgencyCode",
            "AgencyCode"
          },
          {
            "AgencyName",
            "AgencyName"
          },
          {
            "AgentName",
            "AgentName"
          },
          {
            "SetupAgent",
            "SetupAgent"
          },
          {
            "ReservationTypeCode",
            "ReservationTypeCode"
          }
        }
      });
      this._adapter.DeleteCommand = new SqlCommand();
      this._adapter.DeleteCommand.Connection = this.Connection;
      this._adapter.DeleteCommand.CommandText = "dbo.spOriginalBookingDelete";
      this._adapter.DeleteCommand.CommandType = CommandType.StoredProcedure;
      this._adapter.DeleteCommand.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.DeleteCommand.Parameters.Add(new SqlParameter("@OrginalBookingID", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "OrginalBookingID", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand = new SqlCommand();
      this._adapter.InsertCommand.Connection = this.Connection;
      this._adapter.InsertCommand.CommandText = "dbo.spOriginalBookingInsert";
      this._adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@OrginalBookingID", SqlDbType.Int, 4, ParameterDirection.InputOutput, (byte) 10, (byte) 0, "OrginalBookingID", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@FileName", SqlDbType.NVarChar, 200, ParameterDirection.Input, (byte) 0, (byte) 0, "FileName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@FileContent", SqlDbType.NVarChar, 2000, ParameterDirection.Input, (byte) 0, (byte) 0, "FileContent", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@Url", SqlDbType.NVarChar, 1000, ParameterDirection.Input, (byte) 0, (byte) 0, "Url", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "Status", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ID", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime, 8, ParameterDirection.Input, (byte) 23, (byte) 3, "Date", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "Type", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@ReservationNumberOrginal", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationNumberOrginal", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@ReservationNumberUv", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationNumberUv", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@StatusText", SqlDbType.NVarChar, 500, ParameterDirection.Input, (byte) 0, (byte) 0, "StatusText", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@HotelIdUv", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "HotelIdUv", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@HotelName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "HotelName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@ReservationType", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "ReservationType", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@AgencyCode", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "AgencyCode", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@AgencyName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "AgencyName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@AgentName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "AgentName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@SetupAgent", SqlDbType.NVarChar, 50, ParameterDirection.Input, (byte) 0, (byte) 0, "SetupAgent", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.InsertCommand.Parameters.Add(new SqlParameter("@ReservationTypeCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationTypeCode", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand = new SqlCommand();
      this._adapter.UpdateCommand.Connection = this.Connection;
      this._adapter.UpdateCommand.CommandText = "dbo.spOriginalBookingUpdate";
      this._adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrginalBookingID", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "OrginalBookingID", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@FileName", SqlDbType.NVarChar, 200, ParameterDirection.Input, (byte) 0, (byte) 0, "FileName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@FileContent", SqlDbType.NVarChar, 2000, ParameterDirection.Input, (byte) 0, (byte) 0, "FileContent", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@Url", SqlDbType.NVarChar, 1000, ParameterDirection.Input, (byte) 0, (byte) 0, "Url", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "Status", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ID", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime, 8, ParameterDirection.Input, (byte) 23, (byte) 3, "Date", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "Type", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReservationNumberOrginal", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationNumberOrginal", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReservationNumberUv", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationNumberUv", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@StatusText", SqlDbType.NVarChar, 500, ParameterDirection.Input, (byte) 0, (byte) 0, "StatusText", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@HotelIdUv", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "HotelIdUv", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@HotelName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "HotelName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReservationType", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, "ReservationType", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@AgencyCode", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, "AgencyCode", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@AgencyName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "AgencyName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@AgentName", SqlDbType.NVarChar, 100, ParameterDirection.Input, (byte) 0, (byte) 0, "AgentName", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@SetupAgent", SqlDbType.NVarChar, 50, ParameterDirection.Input, (byte) 0, (byte) 0, "SetupAgent", DataRowVersion.Current, false, (object) null, "", "", ""));
      this._adapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReservationTypeCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, (byte) 0, (byte) 0, "ReservationTypeCode", DataRowVersion.Current, false, (object) null, "", "", ""));
    }

    [DebuggerNonUserCode]
    private void InitConnection()
    {
      this._connection = new SqlConnection();
      this._connection.ConnectionString = Settings.Default.OzHotelesConnectionString;
    }

    [DebuggerNonUserCode]
    private void InitCommandCollection()
    {
      this._commandCollection = new SqlCommand[2];
      this._commandCollection[0] = new SqlCommand();
      this._commandCollection[0].Connection = this.Connection;
      this._commandCollection[0].CommandText = "dbo.spOriginalBookingSelectByID";
      this._commandCollection[0].CommandType = CommandType.StoredProcedure;
      this._commandCollection[0].Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[0].Parameters.Add(new SqlParameter("@OrginalBookingID", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1] = new SqlCommand();
      this._commandCollection[1].Connection = this.Connection;
      this._commandCollection[1].CommandText = "dbo.spOriginalBookingSelectByFilter";
      this._commandCollection[1].CommandType = CommandType.StoredProcedure;
      this._commandCollection[1].Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@OrginalBookingID", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@Status", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@ID", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@DateIni", SqlDbType.DateTime, 8, ParameterDirection.Input, (byte) 23, (byte) 3, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@DateFin", SqlDbType.DateTime, 8, ParameterDirection.Input, (byte) 23, (byte) 3, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@Type", SqlDbType.NVarChar, 20, ParameterDirection.Input, (byte) 0, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
      this._commandCollection[1].Parameters.Add(new SqlParameter("@ReservationType", SqlDbType.Int, 4, ParameterDirection.Input, (byte) 10, (byte) 0, (string) null, DataRowVersion.Current, false, (object) null, "", "", ""));
    }

    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    [DataObjectMethod(DataObjectMethodType.Fill, true)]
    public virtual int Fill(EngineLogDataSet.OriginalBookingDataTable dataTable, int? OrginalBookingID)
    {
      this.Adapter.SelectCommand = this.CommandCollection[0];
      if (OrginalBookingID.HasValue)
        this.Adapter.SelectCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.SelectCommand.Parameters[1].Value = (object) DBNull.Value;
      if (this.ClearBeforeFill)
        dataTable.Clear();
      return this.Adapter.Fill((DataTable) dataTable);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    [HelpKeyword("vs.data.TableAdapter")]
    [DebuggerNonUserCode]
    public virtual EngineLogDataSet.OriginalBookingDataTable GetData(int? OrginalBookingID)
    {
      this.Adapter.SelectCommand = this.CommandCollection[0];
      if (OrginalBookingID.HasValue)
        this.Adapter.SelectCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.SelectCommand.Parameters[1].Value = (object) DBNull.Value;
      EngineLogDataSet.OriginalBookingDataTable bookingDataTable = new EngineLogDataSet.OriginalBookingDataTable();
      this.Adapter.Fill((DataTable) bookingDataTable);
      return bookingDataTable;
    }

    [DataObjectMethod(DataObjectMethodType.Fill, false)]
    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    public virtual int FillByFilter(EngineLogDataSet.OriginalBookingDataTable dataTable, int? OrginalBookingID, int? Status, string ID, DateTime? DateIni, DateTime? DateFin, string Type, int? ReservationType)
    {
      this.Adapter.SelectCommand = this.CommandCollection[1];
      if (OrginalBookingID.HasValue)
        this.Adapter.SelectCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.SelectCommand.Parameters[1].Value = (object) DBNull.Value;
      if (Status.HasValue)
        this.Adapter.SelectCommand.Parameters[2].Value = (object) Status.Value;
      else
        this.Adapter.SelectCommand.Parameters[2].Value = (object) DBNull.Value;
      if (ID == null)
        this.Adapter.SelectCommand.Parameters[3].Value = (object) DBNull.Value;
      else
        this.Adapter.SelectCommand.Parameters[3].Value = (object) ID;
      if (DateIni.HasValue)
        this.Adapter.SelectCommand.Parameters[4].Value = (object) DateIni.Value;
      else
        this.Adapter.SelectCommand.Parameters[4].Value = (object) DBNull.Value;
      if (DateFin.HasValue)
        this.Adapter.SelectCommand.Parameters[5].Value = (object) DateFin.Value;
      else
        this.Adapter.SelectCommand.Parameters[5].Value = (object) DBNull.Value;
      if (Type == null)
        this.Adapter.SelectCommand.Parameters[6].Value = (object) DBNull.Value;
      else
        this.Adapter.SelectCommand.Parameters[6].Value = (object) Type;
      if (ReservationType.HasValue)
        this.Adapter.SelectCommand.Parameters[7].Value = (object) ReservationType.Value;
      else
        this.Adapter.SelectCommand.Parameters[7].Value = (object) DBNull.Value;
      if (this.ClearBeforeFill)
        dataTable.Clear();
      return this.Adapter.Fill((DataTable) dataTable);
    }

    [DataObjectMethod(DataObjectMethodType.Select, false)]
    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    public virtual EngineLogDataSet.OriginalBookingDataTable GetDataByFilter(int? OrginalBookingID, int? Status, string ID, DateTime? DateIni, DateTime? DateFin, string Type, int? ReservationType)
    {
      this.Adapter.SelectCommand = this.CommandCollection[1];
      if (OrginalBookingID.HasValue)
        this.Adapter.SelectCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.SelectCommand.Parameters[1].Value = (object) DBNull.Value;
      if (Status.HasValue)
        this.Adapter.SelectCommand.Parameters[2].Value = (object) Status.Value;
      else
        this.Adapter.SelectCommand.Parameters[2].Value = (object) DBNull.Value;
      if (ID == null)
        this.Adapter.SelectCommand.Parameters[3].Value = (object) DBNull.Value;
      else
        this.Adapter.SelectCommand.Parameters[3].Value = (object) ID;
      if (DateIni.HasValue)
        this.Adapter.SelectCommand.Parameters[4].Value = (object) DateIni.Value;
      else
        this.Adapter.SelectCommand.Parameters[4].Value = (object) DBNull.Value;
      if (DateFin.HasValue)
        this.Adapter.SelectCommand.Parameters[5].Value = (object) DateFin.Value;
      else
        this.Adapter.SelectCommand.Parameters[5].Value = (object) DBNull.Value;
      if (Type == null)
        this.Adapter.SelectCommand.Parameters[6].Value = (object) DBNull.Value;
      else
        this.Adapter.SelectCommand.Parameters[6].Value = (object) Type;
      if (ReservationType.HasValue)
        this.Adapter.SelectCommand.Parameters[7].Value = (object) ReservationType.Value;
      else
        this.Adapter.SelectCommand.Parameters[7].Value = (object) DBNull.Value;
      EngineLogDataSet.OriginalBookingDataTable bookingDataTable = new EngineLogDataSet.OriginalBookingDataTable();
      this.Adapter.Fill((DataTable) bookingDataTable);
      return bookingDataTable;
    }

    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    public virtual int Update(EngineLogDataSet.OriginalBookingDataTable dataTable)
    {
      return this.Adapter.Update((DataTable) dataTable);
    }

    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    public virtual int Update(EngineLogDataSet dataSet)
    {
      return this.Adapter.Update((DataSet) dataSet, "OriginalBooking");
    }

    [DebuggerNonUserCode]
    [HelpKeyword("vs.data.TableAdapter")]
    public virtual int Update(DataRow dataRow)
    {
      return this.Adapter.Update(new DataRow[1]{ dataRow });
    }

    [HelpKeyword("vs.data.TableAdapter")]
    [DebuggerNonUserCode]
    public virtual int Update(DataRow[] dataRows)
    {
      return this.Adapter.Update(dataRows);
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    [HelpKeyword("vs.data.TableAdapter")]
    [DebuggerNonUserCode]
    public virtual int Delete(int? OrginalBookingID)
    {
      if (OrginalBookingID.HasValue)
        this.Adapter.DeleteCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.DeleteCommand.Parameters[1].Value = (object) DBNull.Value;
      ConnectionState state = this.Adapter.DeleteCommand.Connection.State;
      if ((this.Adapter.DeleteCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
        this.Adapter.DeleteCommand.Connection.Open();
      try
      {
        return this.Adapter.DeleteCommand.ExecuteNonQuery();
      }
      finally
      {
        if (state == ConnectionState.Closed)
          this.Adapter.DeleteCommand.Connection.Close();
      }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    [HelpKeyword("vs.data.TableAdapter")]
    [DebuggerNonUserCode]
    public virtual int Insert(ref int? OrginalBookingID, string FileName, string FileContent, string Url, int? Status, string ID, DateTime? Date, string Type, string ReservationNumberOrginal, string ReservationNumberUv, string StatusText, int? HotelIdUv, string HotelName, int? ReservationType, string AgencyCode, string AgencyName, string AgentName, string SetupAgent, string ReservationTypeCode)
    {
      if (OrginalBookingID.HasValue)
        this.Adapter.InsertCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.InsertCommand.Parameters[1].Value = (object) DBNull.Value;
      if (FileName == null)
        this.Adapter.InsertCommand.Parameters[2].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[2].Value = (object) FileName;
      if (FileContent == null)
        this.Adapter.InsertCommand.Parameters[3].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[3].Value = (object) FileContent;
      if (Url == null)
        this.Adapter.InsertCommand.Parameters[4].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[4].Value = (object) Url;
      if (Status.HasValue)
        this.Adapter.InsertCommand.Parameters[5].Value = (object) Status.Value;
      else
        this.Adapter.InsertCommand.Parameters[5].Value = (object) DBNull.Value;
      if (ID == null)
        this.Adapter.InsertCommand.Parameters[6].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[6].Value = (object) ID;
      if (Date.HasValue)
        this.Adapter.InsertCommand.Parameters[7].Value = (object) Date.Value;
      else
        this.Adapter.InsertCommand.Parameters[7].Value = (object) DBNull.Value;
      if (Type == null)
        this.Adapter.InsertCommand.Parameters[8].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[8].Value = (object) Type;
      if (ReservationNumberOrginal == null)
        this.Adapter.InsertCommand.Parameters[9].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[9].Value = (object) ReservationNumberOrginal;
      if (ReservationNumberUv == null)
        this.Adapter.InsertCommand.Parameters[10].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[10].Value = (object) ReservationNumberUv;
      if (StatusText == null)
        this.Adapter.InsertCommand.Parameters[11].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[11].Value = (object) StatusText;
      if (HotelIdUv.HasValue)
        this.Adapter.InsertCommand.Parameters[12].Value = (object) HotelIdUv.Value;
      else
        this.Adapter.InsertCommand.Parameters[12].Value = (object) DBNull.Value;
      if (HotelName == null)
        this.Adapter.InsertCommand.Parameters[13].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[13].Value = (object) HotelName;
      if (ReservationType.HasValue)
        this.Adapter.InsertCommand.Parameters[14].Value = (object) ReservationType.Value;
      else
        this.Adapter.InsertCommand.Parameters[14].Value = (object) DBNull.Value;
      if (AgencyCode == null)
        this.Adapter.InsertCommand.Parameters[15].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[15].Value = (object) AgencyCode;
      if (AgencyName == null)
        this.Adapter.InsertCommand.Parameters[16].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[16].Value = (object) AgencyName;
      if (AgentName == null)
        this.Adapter.InsertCommand.Parameters[17].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[17].Value = (object) AgentName;
      if (SetupAgent == null)
        this.Adapter.InsertCommand.Parameters[18].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[18].Value = (object) SetupAgent;
      if (ReservationTypeCode == null)
        this.Adapter.InsertCommand.Parameters[19].Value = (object) DBNull.Value;
      else
        this.Adapter.InsertCommand.Parameters[19].Value = (object) ReservationTypeCode;
      ConnectionState state = this.Adapter.InsertCommand.Connection.State;
      if ((this.Adapter.InsertCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
        this.Adapter.InsertCommand.Connection.Open();
      try
      {
        int num1 = this.Adapter.InsertCommand.ExecuteNonQuery();
        int num2 = this.Adapter.InsertCommand.Parameters[1].Value == null ? 0 : (this.Adapter.InsertCommand.Parameters[1].Value.GetType() != typeof (DBNull) ? 1 : 0);
        OrginalBookingID = num2 != 0 ? new int?((int) this.Adapter.InsertCommand.Parameters[1].Value) : new int?();
        return num1;
      }
      finally
      {
        if (state == ConnectionState.Closed)
          this.Adapter.InsertCommand.Connection.Close();
      }
    }

    [HelpKeyword("vs.data.TableAdapter")]
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    [DebuggerNonUserCode]
    public virtual int Update(int? OrginalBookingID, string FileName, string FileContent, string Url, int? Status, string ID, DateTime? Date, string Type, string ReservationNumberOrginal, string ReservationNumberUv, string StatusText, int? HotelIdUv, string HotelName, int? ReservationType, string AgencyCode, string AgencyName, string AgentName, string SetupAgent, string ReservationTypeCode)
    {
      if (OrginalBookingID.HasValue)
        this.Adapter.UpdateCommand.Parameters[1].Value = (object) OrginalBookingID.Value;
      else
        this.Adapter.UpdateCommand.Parameters[1].Value = (object) DBNull.Value;
      if (FileName == null)
        this.Adapter.UpdateCommand.Parameters[2].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[2].Value = (object) FileName;
      if (FileContent == null)
        this.Adapter.UpdateCommand.Parameters[3].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[3].Value = (object) FileContent;
      if (Url == null)
        this.Adapter.UpdateCommand.Parameters[4].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[4].Value = (object) Url;
      if (Status.HasValue)
        this.Adapter.UpdateCommand.Parameters[5].Value = (object) Status.Value;
      else
        this.Adapter.UpdateCommand.Parameters[5].Value = (object) DBNull.Value;
      if (ID == null)
        this.Adapter.UpdateCommand.Parameters[6].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[6].Value = (object) ID;
      if (Date.HasValue)
        this.Adapter.UpdateCommand.Parameters[7].Value = (object) Date.Value;
      else
        this.Adapter.UpdateCommand.Parameters[7].Value = (object) DBNull.Value;
      if (Type == null)
        this.Adapter.UpdateCommand.Parameters[8].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[8].Value = (object) Type;
      if (ReservationNumberOrginal == null)
        this.Adapter.UpdateCommand.Parameters[9].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[9].Value = (object) ReservationNumberOrginal;
      if (ReservationNumberUv == null)
        this.Adapter.UpdateCommand.Parameters[10].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[10].Value = (object) ReservationNumberUv;
      if (StatusText == null)
        this.Adapter.UpdateCommand.Parameters[11].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[11].Value = (object) StatusText;
      if (HotelIdUv.HasValue)
        this.Adapter.UpdateCommand.Parameters[12].Value = (object) HotelIdUv.Value;
      else
        this.Adapter.UpdateCommand.Parameters[12].Value = (object) DBNull.Value;
      if (HotelName == null)
        this.Adapter.UpdateCommand.Parameters[13].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[13].Value = (object) HotelName;
      if (ReservationType.HasValue)
        this.Adapter.UpdateCommand.Parameters[14].Value = (object) ReservationType.Value;
      else
        this.Adapter.UpdateCommand.Parameters[14].Value = (object) DBNull.Value;
      if (AgencyCode == null)
        this.Adapter.UpdateCommand.Parameters[15].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[15].Value = (object) AgencyCode;
      if (AgencyName == null)
        this.Adapter.UpdateCommand.Parameters[16].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[16].Value = (object) AgencyName;
      if (AgentName == null)
        this.Adapter.UpdateCommand.Parameters[17].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[17].Value = (object) AgentName;
      if (SetupAgent == null)
        this.Adapter.UpdateCommand.Parameters[18].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[18].Value = (object) SetupAgent;
      if (ReservationTypeCode == null)
        this.Adapter.UpdateCommand.Parameters[19].Value = (object) DBNull.Value;
      else
        this.Adapter.UpdateCommand.Parameters[19].Value = (object) ReservationTypeCode;
      ConnectionState state = this.Adapter.UpdateCommand.Connection.State;
      if ((this.Adapter.UpdateCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
        this.Adapter.UpdateCommand.Connection.Open();
      try
      {
        return this.Adapter.UpdateCommand.ExecuteNonQuery();
      }
      finally
      {
        if (state == ConnectionState.Closed)
          this.Adapter.UpdateCommand.Connection.Close();
      }
    }
  }
}
