﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.Service1
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using emailTemplates;
using HtmlAgilityPack;
using OriginalBooking.Data;
using OriginalBooking.Data.EngineLogDataSetTableAdapters;
using Portal.Hotel.Common.Data;
using Portal.Hotel.Facade;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Xml;
using WSHotelCommon;
using WSHotelFacade;

namespace OriginalBooking
{
  public class Service1 : ServiceBase
  {
    private static string noMinutes = ConfigurationManager.AppSettings["NoMinutes"];
    private static string LogPath = ConfigurationManager.AppSettings[nameof (LogPath)];
    private static string UrlDirecctoty = ConfigurationManager.AppSettings[nameof (UrlDirecctoty)];
    private static string ConfigurationFilePath = ConfigurationManager.AppSettings[nameof (ConfigurationFilePath)];
    private static Thread ThFile = (Thread) null;
    private IContainer components = (IContainer) null;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.ServiceName = nameof (Service1);
    }

    public Service1()
    {
      this.InitializeComponent();
      this.CanPauseAndContinue = false;
      this.CanShutdown = true;
      this.CanHandleSessionChangeEvent = true;
      this.ServiceName = "UniVisit_OriginalBooking";
    }

    protected override void OnStart(string[] args)
    {
      Service1.WriteLog("Se intenta iniciar el servicio");
      Service1.IniciarHilo();
    }

    protected override void OnStop()
    {
      try
      {
        Service1.WriteLog("Se intenta detiene el servicio");
        Service1.DetenerHilo();
      }
      catch (Exception ex)
      {
        Service1.WriteLogException(ex);
      }
    }

    private static void IniciarHilo()
    {
      if (Service1.ThFile == null)
      {
        Service1.ThFile = new Thread(new ThreadStart(Service1.ProcesarHilo));
        Service1.ThFile.Start();
        Service1.WriteLog("Se inicia el servicio");
      }
      else
      {
        if (Service1.ThFile.IsAlive)
          return;
        Service1.ThFile = new Thread(new ThreadStart(Service1.ProcesarHilo));
        Service1.ThFile.Start();
        Service1.WriteLog("Se inicia el servicio");
      }
    }

    private static void DetenerHilo()
    {
      if (Service1.ThFile == null || !Service1.ThFile.IsAlive)
        return;
      Service1.ThFile.Abort();
      Service1.WriteLog("Se detiene el servicio");
    }

    private static void ProcesarHilo()
    {
      bool flag = true;
      while (true)
      {
        try
        {
          if (flag)
          {
            Thread.Sleep(120000);
            flag = false;
          }
          Service1.WriteLog(string.Format("Inicia Sincronizacion"));
          try
          {
            string urlDirecctoty = Service1.UrlDirecctoty;
            char[] chArray = new char[1]{ ',' };
            foreach (string str in urlDirecctoty.Split(chArray))
            {
              if (str.Trim() != string.Empty)
              {
                Service1.WriteLog(string.Format("    {0}", (object) str.Trim()));
                Service1.SynchronizeDataTableWithDirectory(str.Trim());
              }
            }
            Service1.WriteLog(string.Format("Finaliza Sincronizacion"));
          }
          catch (Exception ex)
          {
            Service1.WriteLog(string.Format("Finaliza con Error Sincronizacion: {0}", (object) ex.Message));
            Service1.WriteLogException(ex);
          }
          try
          {
            Service1.WriteLog(string.Format("Inicia Procesar los pendientes"));
            Service1.PendingProcessing();
            Service1.WriteLog(string.Format("Finaliza Procesar los pendientes"));
          }
          catch (Exception ex)
          {
            Service1.WriteLog(string.Format("Finaliza con Error Pendientes: {0}", (object) ex.Message));
            Service1.WriteLogException(ex);
          }
          Service1.WriteLog(string.Format("Se duerme el proceso"));
          Thread.Sleep(Service1.GetNoMinuties());
          Service1.WriteLog(string.Format("Se despierta el proceso"));
        }
        catch (Exception ex)
        {
          Service1.WriteLogException(ex);
          Thread.Sleep(Service1.GetNoMinuties());
        }
      }
    }

    private static EngineLogDataSet SynchronizeDataTableWithDirectory(string url)
    {
      EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
      EngineLogDataSet ds = new EngineLogDataSet();
      DateTime dateTime1 = Service1.GetNowDate().AddDays(-2.0);
      DateTime dateTime2 = Service1.GetNowDate().AddDays(1.0);
      string contentOfDirectory1 = Service1.GetContentOfDirectory(url);
      HtmlDocument htmlDocument = new HtmlDocument();
      htmlDocument.LoadHtml(contentOfDirectory1);
      if (htmlDocument != null)
      {
        HtmlNodeCollection htmlNodeCollection = htmlDocument.get_DocumentNode().SelectNodes("//a");
        if (htmlNodeCollection != null)
        {
          using (IEnumerator<HtmlNode> enumerator = ((IEnumerable<HtmlNode>) htmlNodeCollection).GetEnumerator())
          {
            while (((IEnumerator) enumerator).MoveNext())
            {
              HtmlNode current = enumerator.Current;
              Service1.FillEngineLogDataSetByDirectoryNode(ds, current, url);
            }
          }
          int num1 = 0;
          foreach (EngineLogDataSet.EngineLogRow engineLogRow in (TypedTableBase<EngineLogDataSet.EngineLogRow>) ds.EngineLog)
          {
            if (!engineLogRow.IsDateNull() && engineLogRow.Date >= dateTime1 && engineLogRow.Date <= dateTime2)
            {
              ++num1;
              engineLogDataSet.EngineLog.AddEngineLogRow(engineLogRow.FileName, engineLogRow.Content, engineLogRow.Url, 0, engineLogRow.ID, engineLogRow.Date, engineLogRow.ReservationType);
            }
          }
          OriginalBookingTableAdapter bookingTableAdapter = new OriginalBookingTableAdapter();
          bookingTableAdapter.FillByFilter(ds.OriginalBooking, new int?(), new int?(), (string) null, new DateTime?(dateTime1), new DateTime?(dateTime2), (string) null, new int?());
          int num2 = 0;
          foreach (EngineLogDataSet.EngineLogRow engineLogRow in (TypedTableBase<EngineLogDataSet.EngineLogRow>) engineLogDataSet.EngineLog)
          {
            if (ds.OriginalBooking.Select(string.Format("FileName ='{0}'", (object) engineLogRow.FileName)).Length == 0)
            {
              ++num2;
              string contentOfDirectory2 = Service1.GetContentOfDirectory(engineLogRow.Url);
              string Type = "";
              string ReservationNumberOrginal = "";
              string str = "";
              string AgencyName = "";
              string AgencyCode = "";
              string AgentName = "";
              string SetupAgent = "";
              if (engineLogRow.IsReservationTypeNull() || engineLogRow.ReservationType == 1)
              {
                EngineLogDataSet.RequestWebSiteRow dr = engineLogDataSet.RequestWebSite.NewRequestWebSiteRow();
                Service1.LoadRequestWebSite(dr, contentOfDirectory2);
                eReservationType eReservationType = eReservationType.WebSite;
                string ReservationTypeCode = "WEBSITE";
                if (!dr.IsReservationStatusNull())
                  Type = dr.ReservationStatus;
                if (!dr.IsConfirmationNumberNull())
                  ReservationNumberOrginal = dr.ConfirmationNumber;
                if (!dr.IsHotelNull())
                  str = dr.Hotel;
                if (!dr.IsSetupAgentNull())
                  SetupAgent = dr.SetupAgent;
                if (!dr.IsSourceNull())
                  ReservationTypeCode = dr.Source.Trim().ToUpper();
                if (ReservationTypeCode.ToUpper().Trim() != "WEBSITE")
                  eReservationType = eReservationType.Webaffil;
                int hotelId = Service1.GetHotelId(str);
                engineLogDataSet.OriginalBooking.AddOriginalBookingRow(engineLogRow.FileName, contentOfDirectory2, engineLogRow.Url, 0, engineLogRow.ID, engineLogRow.Date, Type, ReservationNumberOrginal, "", "", hotelId, str, (int) eReservationType, AgencyCode, AgencyName, AgentName, SetupAgent, ReservationTypeCode);
              }
              else if (!engineLogRow.IsReservationTypeNull() && engineLogRow.ReservationType == 2)
              {
                EngineLogDataSet.RequestAgencyRow dr = engineLogDataSet.RequestAgency.NewRequestAgencyRow();
                Service1.LoadRequestAgency(dr, contentOfDirectory2);
                string ReservationTypeCode = "AGENCY";
                if (!dr.IsReservationStatusNull())
                  Type = dr.ReservationStatus;
                if (!dr.IsConfirmationNumberNull())
                  ReservationNumberOrginal = dr.ConfirmationNumber;
                if (!dr.IsHotelNull())
                  str = dr.Hotel;
                if (!dr.IsAgencyCodeNull())
                  AgencyCode = dr.AgencyCode;
                if (!dr.IsAgentNameNull())
                  AgentName = dr.AgentName;
                int hotelId = Service1.GetHotelId(str);
                string agencyName = Service1.GetAgencyName(AgencyCode);
                engineLogDataSet.OriginalBooking.AddOriginalBookingRow(engineLogRow.FileName, contentOfDirectory2, engineLogRow.Url, 0, engineLogRow.ID, engineLogRow.Date, Type, ReservationNumberOrginal, "", "", hotelId, str, engineLogRow.ReservationType, AgencyCode, agencyName, AgentName, SetupAgent, ReservationTypeCode);
              }
            }
          }
          if (num2 > 0)
          {
            Service1.WriteLog(string.Format("se encontraron #{0} peticiones que no estaban en la BD", (object) num2));
            bookingTableAdapter.Update(engineLogDataSet.OriginalBooking);
            Service1.WriteLog(string.Format("se actualizo la BD"));
          }
        }
      }
      return engineLogDataSet;
    }

    private static void PendingProcessing()
    {
      OriginalBookingTableAdapter bookingTableAdapter = new OriginalBookingTableAdapter();
      EngineLogDataSet engineLogDataSet = new EngineLogDataSet();
      bookingTableAdapter.FillByFilter(engineLogDataSet.OriginalBooking, new int?(), new int?(0), (string) null, new DateTime?(), new DateTime?(), (string) null, new int?());
      foreach (EngineLogDataSet.OriginalBookingRow dr in (TypedTableBase<EngineLogDataSet.OriginalBookingRow>) engineLogDataSet.OriginalBooking)
      {
        Thread.Sleep(5000);
        if (!dr.IsTypeNull())
        {
          if (dr.ReservationType == 1 || dr.ReservationType == 3)
          {
            if (dr.Type == "Firm/Res")
            {
              EngineLogDataSet.RequestWebSiteRow requestWebSiteRow = engineLogDataSet.RequestWebSite.NewRequestWebSiteRow();
              Service1.LoadRequestWebSite(requestWebSiteRow, dr.FileContent);
              try
              {
                string reservationNumner = "";
                if (Service1.NewSellWebSite(requestWebSiteRow, ref reservationNumner))
                {
                  dr.ReservationNumberUv = reservationNumner;
                  dr.Status = 1;
                  dr.StatusText = "OK";
                  Service1.WriteLog(string.Format("===>OK {0} con {1};{2}", (object) ((eReservationType) dr.ReservationType).ToString(), (object) dr.ReservationNumberOrginal, (object) reservationNumner));
                }
              }
              catch (Exception ex)
              {
                dr.Status = 2;
                dr.StatusText = string.Format("{0}", (object) ex.Message);
                Service1.WriteLog(string.Format("===>Error {0} con {1}", (object) ((eReservationType) dr.ReservationType).ToString(), (object) dr.ReservationNumberOrginal));
                Service1.WriteLog(string.Format("{0}", (object) ex.Message));
                Service1.SendMail(dr, ex.Message);
              }
            }
            else
            {
              dr.Status = 2;
              dr.StatusText = string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type);
              Service1.WriteLog(string.Format("===>Error con {0}", (object) dr.ReservationNumberOrginal));
              Service1.WriteLog(string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type));
              Service1.SendMail(dr, dr.StatusText);
            }
          }
          else if (dr.ReservationType == 2)
          {
            if (dr.Type == "Reservation")
            {
              EngineLogDataSet.RequestAgencyRow requestAgencyRow = engineLogDataSet.RequestAgency.NewRequestAgencyRow();
              Service1.LoadRequestAgency(requestAgencyRow, dr.FileContent);
              try
              {
                string reservationNumner = "";
                if (Service1.NewSellAgency(requestAgencyRow, ref reservationNumner))
                {
                  dr.ReservationNumberUv = reservationNumner;
                  dr.Status = 1;
                  dr.StatusText = "OK";
                  Service1.WriteLog(string.Format("===>OK Agency con {0};{1}", (object) dr.ReservationNumberOrginal, (object) reservationNumner));
                }
              }
              catch (Exception ex)
              {
                dr.Status = 2;
                dr.StatusText = string.Format("{0}", (object) ex.Message);
                Service1.WriteLog(string.Format("===>Error Agency con {0}", (object) dr.ReservationNumberOrginal));
                Service1.WriteLog(string.Format("{0}", (object) ex.Message));
                Service1.SendMail(dr, ex.Message);
              }
            }
            else
            {
              dr.Status = 2;
              dr.StatusText = string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type);
              Service1.WriteLog(string.Format("===>Error con {0}", (object) dr.ReservationNumberOrginal));
              Service1.WriteLog(string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type));
              Service1.SendMail(dr, dr.StatusText);
            }
          }
          else
          {
            dr.Status = 2;
            dr.StatusText = string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type);
            Service1.WriteLog(string.Format("===>Error con {0}", (object) dr.ReservationNumberOrginal));
            Service1.WriteLog(string.Format("El 'Type' es deconocido [{0}]", (object) dr.Type));
            Service1.SendMail(dr, dr.StatusText);
          }
        }
        else
        {
          dr.Status = 2;
          dr.StatusText = "El 'ReservationType' desconocido";
          Service1.WriteLog(string.Format("===>Error con {0}", (object) dr.ReservationNumberOrginal));
          Service1.WriteLog(string.Format("El 'Type' es NULL"));
          Service1.SendMail(dr, dr.StatusText);
        }
        bookingTableAdapter.Update(engineLogDataSet.OriginalBooking);
      }
    }

    private static void SendMail(EngineLogDataSet.OriginalBookingRow dr, string error)
    {
      try
      {
        Service1.WriteLog(string.Format("===>Se Intenta Enviar Correo"));
        Template template = new Template();
        string str1 = string.Format("<b>Contenido:</b><br/>{0}<p></p><b>Error:</b>{1}", (object) dr.FileContent, (object) error);
        string str2 = string.Format("[OriginalBooking] Error en reservacion {0}", (object) dr.ReservationNumberOrginal);
        template.set_Idioma("es-MX");
        template.set_SubjectParam(str2);
        template.set_Html(true);
        template.set_TemplateName("OriginalError");
        template.set_To((object) ConfigurationManager.AppSettings["EmailError"]);
        template.set_AddParameter("INFO", str1);
        try
        {
          if (template.SendWithoutErrorControl("", "", ""))
          {
            Service1.WriteLog(string.Format("===>Se Envio Correo"));
            return;
          }
        }
        catch (Exception ex)
        {
          Service1.WriteLogException(ex);
        }
        Service1.WriteLog(string.Format("===>No se envio"));
      }
      catch (Exception ex)
      {
        Service1.WriteLog(string.Format("===>Correo Error con {0}", (object) ex.Message));
      }
    }

    private static bool NewSellWebSite(EngineLogDataSet.RequestWebSiteRow drRequest, ref string reservationNumner)
    {
      if (drRequest != null)
      {
        int hotelId = Service1.GetHotelId(drRequest.Hotel);
        if (hotelId == -1)
          throw new ExceptionOriginalBooking(string.Format("HotelId = -1"));
        reqHotelSell rSell = new reqHotelSell();
        resHotelSell resHotelSell = new resHotelSell();
        XmlDocument xmlDocument = new XmlDocument();
        string empty = string.Empty;
        reqHotelSell.HotelSellRow hotelSellRow = rSell.get_HotelSell().NewHotelSellRow();
        rSell.get_HotelSell().AddHotelSellRow(hotelSellRow);
        reqHotelSell.HotelHeaderRow hotelHeaderRow = rSell.get_HotelHeader().NewHotelHeaderRow();
        hotelHeaderRow.set_Language("en-US");
        hotelHeaderRow.set_Source("HTL");
        rSell.get_HotelHeader().AddHotelHeaderRow(hotelHeaderRow);
        ((DataRow) hotelHeaderRow).SetParentRow((DataRow) hotelSellRow);
        reqHotelSell.ReservationRow reservationRow = rSell.get_Reservation().NewReservationRow();
        if (!drRequest.IsCheckInNull() && drRequest.CheckIn.Length == 10)
          reservationRow.set_CheckInDate(drRequest.CheckIn.Substring(0, 4) + drRequest.CheckIn.Substring(5, 2) + drRequest.CheckIn.Substring(8, 2));
        if (!drRequest.IsCheckOutNull() && drRequest.CheckOut.Length == 10)
          reservationRow.set_CheckOutDate(drRequest.CheckOut.Substring(0, 4) + drRequest.CheckOut.Substring(5, 2) + drRequest.CheckOut.Substring(8, 2));
        reservationRow.set_RateCode(Service1.GetRateCode(hotelId, drRequest.RoomName));
        if (reservationRow.get_RateCode().Trim() == string.Empty)
          throw new ExceptionOriginalBooking(string.Format("drR.RateCode = 'vacio'; HotelName = {0}; con Hotel ID = {1}; RoomName = {2}", (object) drRequest.Hotel, (object) hotelId, (object) drRequest.RoomName));
        reservationRow.set_PropertyNumber((long) hotelId);
        reservationRow.set_Total(drRequest.Total);
        string str = !(drRequest.CurrencyCode.Trim().ToUpper() == "Mexican Pesos".ToUpper()) ? (!(drRequest.CurrencyCode.Trim().ToUpper() == "MEX".ToUpper()) ? (!(drRequest.CurrencyCode.Trim().ToUpper() == "Pesos Mexicanos".ToUpper()) ? drRequest.CurrencyCode : "MXN") : "MXN") : "MXN";
        reservationRow.set_Money(str);
        reservationRow.set_ChainCode("UV");
        reservationRow.set_RAwayAdult("0");
        reservationRow.set_RAwayChild("0");
        reservationRow.set_RAwayCrib("0");
        rSell.get_Reservation().AddReservationRow(reservationRow);
        ((DataRow) reservationRow).SetParentRow((DataRow) hotelSellRow);
        int result1 = 0;
        int result2 = 0;
        int result3 = 0;
        reqHotelSell.RoomsRow drhrs = rSell.get_Rooms().NewRoomsRow();
        rSell.get_Rooms().AddRoomsRow(drhrs);
        ((DataRow) drhrs).SetParentRow((DataRow) hotelSellRow);
        int.TryParse(drRequest.Rooms, out result3);
        int.TryParse(drRequest.Adults, out result1);
        int.TryParse(drRequest.Children, out result2);
        Service1.CreateRooms(rSell, drhrs, result1, result2, result3, drRequest.SpecialRequest);
        reqHotelSell.CustomerRow customerRow = rSell.get_Customer().NewCustomerRow();
        customerRow.set_FirstName(drRequest.FirstName == string.Empty ? "?" : drRequest.FirstName);
        customerRow.set_LastName(drRequest.LastName == string.Empty ? "?" : drRequest.LastName);
        customerRow.set_PhoneHome(drRequest.Telephone);
        customerRow.set_Address(drRequest.Adress);
        customerRow.set_Email(drRequest.Email);
        customerRow.set_Estate(drRequest.Estate);
        customerRow.set_City(drRequest.City);
        customerRow.set_Country(drRequest.Country.Trim().ToUpper() == "Mexico".ToUpper() ? "MX" : drRequest.Country);
        customerRow.set_PostalCode(drRequest.PostalCode);
        rSell.get_Customer().AddCustomerRow(customerRow);
        ((DataRow) customerRow).SetParentRow((DataRow) hotelSellRow);
        xmlDocument.LoadXml(((DataSet) rSell).GetXml());
        if (str != "MXN" && str != "USD")
          throw new ExceptionOriginalBooking(string.Format("Moneda no soportada \"{0}\"", (object) str));
        resHotelSell hotelSell = new clsFASELL().GetHotelSell(xmlDocument.DocumentElement);
        if (hotelSell != null)
        {
          if (hotelSell.get__Error().get_Count() != 0)
            throw new ExceptionOriginalBooking(string.Format("Error en la peticion: {0}", (object) hotelSell.get__Error().get_Item(0).get_Message().ToString()));
          if (hotelSell.get_Reservation().get_Count() <= 0)
            throw new ExceptionOriginalBooking(string.Format("No se encontro en la respuesta la tabla Reservation"));
          reservationNumner = hotelSell.get_Reservation().get_Item(0).get_ConfirmNumber();
          return true;
        }
      }
      return false;
    }

    private static bool NewSellAgency(EngineLogDataSet.RequestAgencyRow drRequest, ref string reservationNumner)
    {
      if (drRequest != null)
      {
        int hotelId = Service1.GetHotelId(drRequest.Hotel);
        if (hotelId == -1)
          throw new ExceptionOriginalBooking(string.Format("HotelId = -1"));
        reqHotelSell rSell = new reqHotelSell();
        resHotelSell resHotelSell = new resHotelSell();
        XmlDocument xmlDocument = new XmlDocument();
        string empty = string.Empty;
        reqHotelSell.HotelSellRow hotelSellRow = rSell.get_HotelSell().NewHotelSellRow();
        rSell.get_HotelSell().AddHotelSellRow(hotelSellRow);
        reqHotelSell.HotelHeaderRow hotelHeaderRow = rSell.get_HotelHeader().NewHotelHeaderRow();
        hotelHeaderRow.set_Language("en-US");
        hotelHeaderRow.set_Source("HTL");
        rSell.get_HotelHeader().AddHotelHeaderRow(hotelHeaderRow);
        ((DataRow) hotelHeaderRow).SetParentRow((DataRow) hotelSellRow);
        reqHotelSell.ReservationRow reservationRow = rSell.get_Reservation().NewReservationRow();
        if (!drRequest.IsCheckInNull() && drRequest.CheckIn.Length == 10)
          reservationRow.set_CheckInDate(drRequest.CheckIn.Substring(0, 4) + drRequest.CheckIn.Substring(5, 2) + drRequest.CheckIn.Substring(8, 2));
        if (!drRequest.IsCheckOutNull() && drRequest.CheckOut.Length == 10)
          reservationRow.set_CheckOutDate(drRequest.CheckOut.Substring(0, 4) + drRequest.CheckOut.Substring(5, 2) + drRequest.CheckOut.Substring(8, 2));
        reservationRow.set_RateCode(Service1.GetRateCode(hotelId, drRequest.RoomName));
        if (reservationRow.get_RateCode().Trim() == string.Empty)
          throw new ExceptionOriginalBooking(string.Format("drR.RateCode = 'vacio';HotelName = {0} ;con Hotel ID = {1};RoomName = {2}", (object) drRequest.Hotel, (object) hotelId, (object) drRequest.RoomName));
        reservationRow.set_PropertyNumber((long) hotelId);
        reservationRow.set_Total(drRequest.Total);
        string currency = drRequest.Currency;
        reservationRow.set_Money(currency);
        reservationRow.set_ChainCode("UV");
        reservationRow.set_RAwayAdult("0");
        reservationRow.set_RAwayChild("0");
        reservationRow.set_RAwayCrib("0");
        rSell.get_Reservation().AddReservationRow(reservationRow);
        ((DataRow) reservationRow).SetParentRow((DataRow) hotelSellRow);
        int result1 = 0;
        int result2 = 0;
        int result3 = 0;
        reqHotelSell.RoomsRow drhrs = rSell.get_Rooms().NewRoomsRow();
        rSell.get_Rooms().AddRoomsRow(drhrs);
        ((DataRow) drhrs).SetParentRow((DataRow) hotelSellRow);
        int.TryParse(drRequest.Rooms, out result3);
        int.TryParse(drRequest.Adults, out result1);
        int.TryParse(drRequest.Childrens, out result2);
        Service1.CreateRooms(rSell, drhrs, result1, result2, result3, string.Empty);
        reqHotelSell.CustomerRow customerRow = rSell.get_Customer().NewCustomerRow();
        customerRow.set_FirstName(drRequest.FirstName == string.Empty ? "?" : drRequest.FirstName);
        customerRow.set_LastName(drRequest.LastName == string.Empty ? "?" : drRequest.LastName);
        customerRow.set_PhoneHome("");
        customerRow.set_Address("");
        customerRow.set_Email("");
        customerRow.set_Estate("");
        customerRow.set_City("");
        customerRow.set_Country("");
        customerRow.set_PostalCode("");
        rSell.get_Customer().AddCustomerRow(customerRow);
        ((DataRow) customerRow).SetParentRow((DataRow) hotelSellRow);
        xmlDocument.LoadXml(((DataSet) rSell).GetXml());
        if (currency != "MXN" && currency != "USD")
          throw new ExceptionOriginalBooking(string.Format("Moneda no soportada \"{0}\"", (object) currency));
        resHotelSell hotelSell = new clsFASELL().GetHotelSell(xmlDocument.DocumentElement);
        if (hotelSell != null)
        {
          if (hotelSell.get__Error().get_Count() != 0)
            throw new ExceptionOriginalBooking(string.Format("Error en la peticion: {0}", (object) hotelSell.get__Error().get_Item(0).get_Message().ToString()));
          if (hotelSell.get_Reservation().get_Count() <= 0)
            throw new ExceptionOriginalBooking(string.Format("No se encontro en la respuesta la tabla Reservation"));
          reservationNumner = hotelSell.get_Reservation().get_Item(0).get_ConfirmNumber();
          return true;
        }
      }
      return false;
    }

    private static void CreateRooms(reqHotelSell rSell, reqHotelSell.RoomsRow drhrs, int adultos, int niños, int cuartos, string Preferences)
    {
      for (int index = 0; index <= cuartos - 1; ++index)
      {
        reqHotelSell.RoomRow roomRow = rSell.get_Room().NewRoomRow();
        roomRow.set_Preferences(Preferences);
        roomRow.set_Adults((byte) 1);
        --adultos;
        if (niños > 0)
        {
          roomRow.set_Children((byte) 1);
          --niños;
        }
        else
          roomRow.set_Children((byte) 0);
        rSell.get_Room().AddRoomRow(roomRow);
        ((DataRow) roomRow).SetParentRow((DataRow) drhrs);
      }
      while (adultos > 0 | niños > 0)
      {
        foreach (reqHotelSell.RoomRow roomRow1 in drhrs.GetRoomRows())
        {
          if (adultos > 0)
          {
            reqHotelSell.RoomRow roomRow2 = roomRow1;
            roomRow2.set_Adults((byte) ((uint) roomRow2.get_Adults() + 1U));
            --adultos;
          }
          if (niños > 0)
          {
            reqHotelSell.RoomRow roomRow2 = roomRow1;
            roomRow2.set_Children((byte) ((uint) roomRow2.get_Children() + 1U));
            --niños;
          }
        }
      }
    }

    private static DateTime GetNowDate()
    {
      DateTime now = DateTime.Now;
      int year = now.Year;
      now = DateTime.Now;
      int month = now.Month;
      now = DateTime.Now;
      int day = now.Day;
      int hour = 0;
      int minute = 0;
      int second = 0;
      return new DateTime(year, month, day, hour, minute, second);
    }

    private static string GetContentOfDirectory(string url)
    {
      return new StreamReader(WebRequest.Create(url).GetResponse().GetResponseStream()).ReadToEnd();
    }

    private static void FillEngineLogDataSetByDirectoryNode(EngineLogDataSet ds, HtmlNode node, string baseUrl)
    {
      string innerText = node.get_InnerText();
      if (!innerText.Contains(".txt"))
        return;
      string[] strArray1 = innerText.Split('_');
      if (strArray1.Length == 3 && !innerText.Contains("AGE"))
      {
        EngineLogDataSet.EngineLogRow row = ds.EngineLog.NewEngineLogRow();
        row.FileName = innerText;
        row.Url = string.Format("{0}{1}", (object) baseUrl, (object) innerText);
        if (strArray1[0].Length == 8)
          row.Date = new DateTime(int.Parse(strArray1[0].Substring(0, 4)), int.Parse(strArray1[0].Substring(4, 2)), int.Parse(strArray1[0].Substring(6, 2)));
        string[] strArray2 = strArray1[2].Split('.');
        if (strArray2.Length == 2)
          row.ID = strArray2[0];
        row.Content = "";
        row.ReservationType = 1;
        ds.EngineLog.AddEngineLogRow(row);
      }
      else if (strArray1.Length == 4 && innerText.Contains("AGE"))
      {
        EngineLogDataSet.EngineLogRow row = ds.EngineLog.NewEngineLogRow();
        row.FileName = innerText;
        row.Url = string.Format("{0}{1}", (object) baseUrl, (object) innerText);
        if (strArray1[0].Length == 8)
          row.Date = new DateTime(int.Parse(strArray1[0].Substring(0, 4)), int.Parse(strArray1[0].Substring(4, 2)), int.Parse(strArray1[0].Substring(6, 2)));
        string[] strArray2 = strArray1[3].Split('.');
        if (strArray2.Length == 2)
          row.ID = strArray2[0];
        row.Content = "";
        row.ReservationType = 2;
        ds.EngineLog.AddEngineLogRow(row);
      }
    }

    private static void LoadRequestWebSite(EngineLogDataSet.RequestWebSiteRow dr, string request)
    {
      string[] strArray = request.Split('|');
      if (strArray.Length <= 1)
        return;
      for (int index = 0; index < strArray.Length; ++index)
      {
        if (index <= dr.Table.Columns.Count - 1)
          dr[index] = (object) strArray[index];
      }
    }

    private static void LoadRequestAgency(EngineLogDataSet.RequestAgencyRow dr, string request)
    {
      string[] strArray = request.Split('|');
      if (strArray.Length <= 1)
        return;
      for (int index = 0; index < strArray.Length; ++index)
      {
        if (index <= dr.Table.Columns.Count - 1)
          dr[index] = (object) strArray[index];
      }
    }

    private static string GetAgencyName(string AgencyCode)
    {
      try
      {
        string fileName = string.Format("{0}Agencies.xml", (object) Service1.ConfigurationFilePath);
        DataSet dataSet = new DataSet();
        string empty = string.Empty;
        int num = (int) dataSet.ReadXml(fileName);
        DataRow[] dataRowArray = dataSet.Tables["Agency"].Select(string.Format("Code = '{0}'", (object) AgencyCode));
        if (dataRowArray.Length > 0)
          return dataRowArray[0]["Name"].ToString();
      }
      catch (Exception ex)
      {
        Service1.WriteLogException(ex);
      }
      return string.Empty;
    }

    private static int GetHotelId(string hotelName)
    {
      try
      {
        string fileName = string.Format("{0}Configuration.xml", (object) Service1.ConfigurationFilePath);
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(fileName);
        DataRow[] dataRowArray = dataSet.Tables["Hotel"].Select(string.Format("Name = '{0}'", (object) hotelName));
        if (dataRowArray.Length > 0)
          return int.Parse(dataRowArray[0]["HotelIdUv"].ToString());
      }
      catch (Exception ex)
      {
        Service1.WriteLogException(ex);
      }
      return -1;
    }

    private static string GetRateCode(int hotelID, string roomName)
    {
      try
      {
        string fileName = string.Format("{0}Configuration.xml", (object) Service1.ConfigurationFilePath);
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(fileName);
        DataRow[] dataRowArray = dataSet.Tables["Room"].Select(string.Format("HotelIdUv = '{0}' AND RoomCodeOriginal = '{1}'", (object) hotelID, (object) roomName));
        if (dataRowArray.Length > 0)
          return dataRowArray[0]["RateCodeUv"].ToString();
      }
      catch (Exception ex)
      {
        Service1.WriteLogException(ex);
      }
      return "";
    }

    private static int GetHotelRoomID_UV(int hotelID, string code)
    {
      try
      {
        RoomsHotelData rooms = new RoomFacade().getRooms(hotelID);
        if (rooms != null && ((DataSet) rooms).Tables["TipoHabitaciones_Hotel"].Rows.Count > 0)
        {
          DataRow[] dataRowArray = ((DataSet) rooms).Tables["TipoHabitaciones_Hotel"].Select(string.Format("CodigoHabitacion = '{0}'", (object) code));
          if (dataRowArray != null && dataRowArray.Length > 0)
            return (int) dataRowArray[0]["idTipoHabitacion"];
        }
      }
      catch (Exception ex)
      {
        Service1.WriteLogException(ex);
      }
      return -1;
    }

    public static void WriteLogException(Exception ex)
    {
      try
      {
        string logPath = Service1.LogPath;
        string str1 = "LogOriginalBookingException";
        string str2 = DateTime.Now.ToString() + " " + ex.Message.ToString() + " " + ex.StackTrace;
        StreamWriter streamWriter = new StreamWriter(logPath + str1 + DateTime.Now.ToString("ddMMyy") + ".log", true);
        streamWriter.WriteLine(str2);
        streamWriter.Flush();
        streamWriter.Close();
      }
      catch (Exception ex1)
      {
        string message = ex1.Message;
      }
    }

    public static void WriteLog(string msg)
    {
      try
      {
        string logPath = Service1.LogPath;
        string str1 = "LogOriginalBooking";
        DateTime now = DateTime.Now;
        msg = now.ToString() + " " + msg;
        string str2 = logPath;
        string str3 = str1;
        now = DateTime.Now;
        string str4 = now.ToString("ddMMyy");
        string str5 = ".log";
        StreamWriter streamWriter = new StreamWriter(str2 + str3 + str4 + str5, true);
        streamWriter.WriteLine(msg);
        streamWriter.Flush();
        streamWriter.Close();
      }
      catch (Exception ex)
      {
        string message = ex.Message;
      }
    }

    private static int GetNoMinuties()
    {
      try
      {
        return 60000 * int.Parse(Service1.noMinutes);
      }
      catch
      {
      }
      return 60000;
    }
  }
}
