﻿// Decompiled with JetBrains decompiler
// Type: OriginalBooking.Properties.Settings
// Assembly: OriginalBooking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 842A5BF7-DFE1-439C-8377-876BB8AE83FF
// Assembly location: \\Mac\Home\Desktop\OriginalBooking.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace OriginalBooking.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        return defaultInstance;
      }
    }

    [ApplicationScopedSetting]
    [DefaultSettingValue("Data Source=sql.univisit.net;Initial Catalog=OzHoteles;Persist Security Info=True;User ID=UserUv; Password=AllUv07")]
    [DebuggerNonUserCode]
    [SpecialSetting(SpecialSetting.ConnectionString)]
    public string OzHotelesConnectionString
    {
      get
      {
        return (string) this[nameof (OzHotelesConnectionString)];
      }
    }
  }
}
